# Message Broker

This is an abstraction of a message broker system. It supports various implementations, such as AWS SQS and AWS SNS.
Given the complexity of the class hierarchy, it's recommended to use the [symfony bundle](https://gitlab.com/talentrydev/message-broker-bundle)
instead of using this library directly.

## Development

- Run `make up` to spin up docker containers
- Run `make deps` to install composer dependencies
- Run `make tests` to run PHPUnit tests
