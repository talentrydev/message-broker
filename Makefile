up:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker up -d
down:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker down
test:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker exec php-cli vendor/bin/phpunit
cs:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker exec php-cli vendor/bin/phpcs --standard=PSR12 src tests
cs-fix:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker exec php-cli vendor/bin/phpcbf --standard=PSR12 src tests
deps:
	docker run -v $(shell pwd):/app --rm -t composer install --ignore-platform-reqs
