<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests;

use Aws\Sns\SnsClient;
use Aws\Sqs\SqsClient;
use Exception;
use InvalidArgumentException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Predis\ClientInterface;
use Psr\Log\NullLogger;
use Talentry\MessageBroker\ApplicationInterface\Message\Message as MessageInterface;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Domain\Message\VisibilityTimeout;
use Talentry\MessageBroker\Infrastructure\ArrayMessageBroker;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\VoidFailedMessageSpool;
use Talentry\MessageBroker\Infrastructure\MessageParser\FallbackMessageParser;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sns\SnsMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Sns\SnsPayloadGenerator;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageAttributesProvider;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageMapper;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageSubscriber;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueueFactory;

class MessageBrokerIntegrationTest extends TestCase
{
    private VisibilityTimeout $visibilityTimeout;
    private string $channelName = 'test';
    private Channel $channel;
    private ?SqsClient $sqsClient = null;
    private ?SnsClient $snsClient = null;
    private MessagePublisher $messagePublisher;
    private MessageSubscriber $messageSubscriber;
    private ?SqsQueueFactory $sqsQueueFactory = null;
    /**
     * @var string[]
     */
    private array $messagePublisherImplementations;
    /**
     * @var string[]
     */
    private array $messageSubscriberImplementations;
    /**
     * @var Message[]
     */
    private array $sentMessages = [];
    /**
     * @var Message[]
     */
    private array $receivedMessages = [];
    private ?string $snsTopicArn = null;
    private Exception $caughtException;
    private int $sqsVisibilityTimeout = 1;
    private $dlqSuffix = '_dead_letter_queue';

    protected function setUp(): void
    {
        $awsArguments = [
            'region' => 'eu-central-1',
            'version' => 'latest',
            'credentials' => [
                'key' => 'key',
                'secret' => 'secret',
            ],
        ];

        $sqsEndpoint = $_ENV['MESSAGE_BROKER_SQS_ENDPOINT'] ?? getenv('MESSAGE_BROKER_SQS_ENDPOINT');
        if ($sqsEndpoint !== false) {
            $awsArguments['endpoint'] = $sqsEndpoint;
            $this->sqsClient = new SqsClient($awsArguments);
        }

        $snsEndpoint = $_ENV['MESSAGE_BROKER_SNS_ENDPOINT'] ?? getenv('MESSAGE_BROKER_SNS_ENDPOINT');
        if ($snsEndpoint !== false) {
            $awsArguments['endpoint'] = $snsEndpoint;
            $this->snsClient = new SnsClient($awsArguments);
        }

        $this->cleanup();

        $arrayBroker = new ArrayMessageBroker();
        $this->messagePublisherImplementations = [$arrayBroker];
        $this->messageSubscriberImplementations = [$arrayBroker];

        $messageParser = new FallbackMessageParser();
        $sqsMessageAttributesProvider = new SqsMessageAttributesProvider($messageParser);
        $sqsMessageMapper = new SqsMessageMapper();
        $logger = new NullLogger();
        $channelRegistry = new ChannelRegistry($this->dlqSuffix);
        if ($this->sqsClient !== null) {
            $this->sqsQueueFactory = new SqsQueueFactory(
                sqsClient: $this->sqsClient,
                channelRegistry: $channelRegistry,
            );
            $sqsMessagePublisher = new SqsMessagePublisher(
                $this->sqsClient,
                $sqsMessageMapper,
                $sqsMessageAttributesProvider,
                $this->sqsQueueFactory,
                new VoidFailedMessageSpool(),
                $logger,
                $channelRegistry,
            );

            $sqsMessageSubscriber = new SqsMessageSubscriber(
                $this->sqsClient,
                $this->sqsQueueFactory,
                $logger,
                $sqsMessageMapper,
                $channelRegistry,
            );

            $this->messagePublisherImplementations[] = $sqsMessagePublisher;
            $this->messageSubscriberImplementations[] = $sqsMessageSubscriber;
        }

        if ($this->snsClient !== null) {
            $result = $this->snsClient->createTopic(['Name' => 'messages']);
            $this->snsTopicArn = $result['TopicArn'];
            $snsPayloadGenerator = new SnsPayloadGenerator(
                $logger,
                $sqsMessageAttributesProvider,
                $channelRegistry,
                $this->createMock(ClientInterface::class),
            );
            $snsMessagePublisher = new SnsMessagePublisher(
                $this->snsClient,
                new VoidFailedMessageSpool(),
                $snsPayloadGenerator,
                $logger,
                $this->snsTopicArn,
            );

            $this->messagePublisherImplementations[] = $snsMessagePublisher;
            $this->channel = new Channel(name: $this->channelName);
        }
    }

    protected function tearDown(): void
    {
        $this->cleanup();
        if (isset($this->caughtException)) {
            throw $this->caughtException;
        }
    }

    #[DataProvider('messageBrokerConfigurationProvider')]
    public function testReceiveMessageWithImmediateDeletion(string $messagePublisher, string $messageSubscriber): void
    {
        $this->givenMessagePublisher($messagePublisher);
        $this->givenMessageSubscriber($messageSubscriber);
        if ($messagePublisher === 'sns') {
            $this->givenThatSnsSubscriptionExist();
        }
        $this->givenThatMessageWasSent();
        usleep(10000);

        $this->whenMessageIsReceivedWithDeletion();
        $this->expectSentMessagesToMatchReceivedMessages();

        //subsequent requests will return no messages
        $this->givenThatVisibilityTimeoutHasElapsed();
        $this->whenMessageIsReceivedWithDeletion();
        $this->expectNoMessageToBeReceived();
    }

    #[DataProvider('messageBrokerConfigurationProvider')]
    public function testReceiveMessageWithoutImmediateDeletion(
        string $messagePublisher,
        string $messageSubscriber
    ): void {
        $this->givenMessagePublisher($messagePublisher);
        $this->givenMessageSubscriber($messageSubscriber);
        if ($messagePublisher === 'sns') {
            $this->givenThatSnsSubscriptionExist();
        }
        $this->givenThatMessageWasSent();

        $this->whenMessageIsReceivedWithoutDeletion();
        $this->expectSentMessagesToMatchReceivedMessages();

        //subsequent requests will return no messages
        $this->whenMessageIsReceivedWithoutDeletion();
        $this->expectNoMessageToBeReceived();
    }

    #[DataProvider('messageBrokerConfigurationProvider')]
    public function testDeleteMessage(string $messagePublisher, string $messageSubscriber): void
    {
        $this->givenMessagePublisher($messagePublisher);
        $this->givenMessageSubscriber($messageSubscriber);
        if ($messagePublisher === 'sns') {
            $this->givenThatSnsSubscriptionExist();
        }
        $this->givenThatMessageWasSent();

        $this->whenMessageIsReceivedWithoutDeletion();
        $this->expectSentMessagesToMatchReceivedMessages();

        $this->whenReceivedMessageIsDeleted();
        $this->whenMessageIsReceivedWithoutDeletion();
        $this->expectNoMessageToBeReceived();

        $this->givenThatVisibilityTimeoutHasElapsed();
        $this->whenMessageIsReceivedWithoutDeletion();
        $this->expectNoMessageToBeReceived();
    }

    #[DataProvider('messageBrokerConfigurationProvider')]
    public function testReceiveMessageFromEmptyChannel(string $messagePublisher, string $messageSubscriber): void
    {
        $this->givenMessagePublisher($messagePublisher);
        $this->givenMessageSubscriber($messageSubscriber);
        if ($messagePublisher === 'sns') {
            $this->givenThatSnsSubscriptionExist();
        }
        $this->whenMessageIsReceivedWithDeletion();
        $this->expectNoMessageToBeReceived();
    }

    #[DataProvider('messageBrokerConfigurationProvider')]
    public function testGetAllMessagesFromChannel(string $messagePublisher, string $messageSubscriber): void
    {
        $this->givenMessagePublisher($messagePublisher);
        $this->givenMessageSubscriber($messageSubscriber);
        if ($messagePublisher === 'sns') {
            $this->givenThatSnsSubscriptionExist();
        }
        $this->givenThatMessageWasSent();
        $this->givenThatMessageWasSent();
        $this->whenAllMessagesAreReceivedWithoutDeletion();
        $this->expectSentMessagesToMatchReceivedMessages();
    }

    #[DataProvider('messageBrokerConfigurationProviderForDeadLetterQueueTest')]
    public function testDeadLetterQueue(string $messagePublisher, string $messageSubscriber): void
    {
        $this->givenMessagePublisher($messagePublisher);
        $this->givenMessageSubscriber($messageSubscriber);
        if ($messagePublisher === 'sns') {
            $this->givenThatSnsSubscriptionExist();
        }
        $this->givenThatMessageWasSent();

        //at first, DLQ should be empty
        $this->whenMessageIsReceivedFromDeadLetterQueue();
        $this->expectNoMessageToBeReceived();

        /**
         * Even though maxReceiveCount is set to 1, we still need to poll for messages at least twice;
         * see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-dead-letter-queues.html
         * "For example, if the source queue has a redrive policy with maxReceiveCount set to 5,
         * and the consumer of the source queue receives a message 6 times without ever deleting it,
         * Amazon SQS moves the message to the dead-letter queue."
         */
        $this->whenMessageIsReceivedWithoutDeletion();
        $this->expectSentMessagesToMatchReceivedMessages();

        $this->givenThatVisibilityTimeoutHasElapsed();
        $this->whenMessageIsReceivedWithoutDeletion();
        $this->expectNoMessageToBeReceived();

        $this->givenThatVisibilityTimeoutHasElapsed();
        $this->whenMessageIsReceivedFromDeadLetterQueue();
        $this->expectSentMessagesToMatchReceivedMessages();

        //test deleting from DLQ
        $this->whenReceivedMessageIsDeleted(true);
        $this->whenMessageIsReceivedFromDeadLetterQueue();
        $this->expectNoMessageToBeReceived();
    }

    #[DataProvider('messageBrokerConfigurationProvider')]
    public function testBatchSendMessages(string $messagePublisher, string $messageSubscriber): void
    {
        $this->givenMessagePublisher($messagePublisher);
        $this->givenMessageSubscriber($messageSubscriber);
        if ($messagePublisher === 'sns') {
            $this->givenThatSnsSubscriptionExist();
        }
        $this->givenThatRandomlyGeneratedMessagesWereSentInABatch(10);
        $this->whenAllMessagesAreReceivedWithoutDeletion();
        $this->expectSentMessagesToMatchReceivedMessages();
    }

    #[DataProvider('messageBrokerConfigurationProvider')]
    public function testBatchSendingInvalidMessages(string $messagePublisher, string $messageSubscriber): void
    {
        $this->givenMessagePublisher($messagePublisher);
        $this->givenMessageSubscriber($messageSubscriber);
        if ($messagePublisher === 'sns') {
            $this->givenThatSnsSubscriptionExist();
        }
        $this->givenThatMessagesWereSentInABatch(['foo', 'bar']);
        $this->expectExceptionToBeCaught(
            InvalidArgumentException::class,
            sprintf('Invalid type: string provided, %s expected', MessageInterface::class)
        );
    }

    private function givenMessagePublisher(string $messagePublisherName): void
    {
        $messagePublisher = null;
        foreach ($this->messagePublisherImplementations as $messagePublisherImplementation) {
            if ($messagePublisherImplementation->name() === $messagePublisherName) {
                $messagePublisher = $messagePublisherImplementation;
                break;
            }
        }

        if ($messagePublisher === null) {
            self::markTestSkipped(sprintf('MessagePublisher %s not available', $messagePublisherName));
        }

        $this->messagePublisher = $messagePublisher;
    }

    private function givenMessageSubscriber(string $messageSubscriberName): void
    {
        $messageSubscriber = null;
        foreach ($this->messageSubscriberImplementations as $messageSubscriberImplementation) {
            if ($messageSubscriberImplementation->name() === $messageSubscriberName) {
                $messageSubscriber = $messageSubscriberImplementation;
                break;
            }
        }

        if ($messageSubscriber === null) {
            self::markTestSkipped(sprintf('MessageSubscriber %s not available', $messageSubscriberName));
        }

        $this->visibilityTimeout = VisibilityTimeout::defaultValue();
        //since we cannot fake the passing of time with SQS service bus, we will use a visibility timeout of 0
        if ($messageSubscriber instanceof SqsMessageSubscriber) {
            $this->visibilityTimeout = VisibilityTimeout::inSeconds($this->sqsVisibilityTimeout);
        }

        $this->messageSubscriber = $messageSubscriber;
    }

    private function givenThatVisibilityTimeoutHasElapsed(): void
    {
        //in case of SQS service bus we cannot fake the passing of time
        if ($this->messageSubscriber instanceof SqsMessageSubscriber) {
            sleep($this->sqsVisibilityTimeout + 1); //adding 1 second to visibility timeout to be safe
        }
    }

    private function givenThatSnsSubscriptionExist(): void
    {
        if ($this->sqsClient !== null && $this->snsClient !== null) {
            //SQS queue must exist in order to create a subscription
            //(can't be created on the fly as in sqs-sqs implementation)
            $sqsQueue = $this->sqsQueueFactory->fromChannel($this->channel);
            $this->snsClient->subscribe([
                'Protocol' => 'sqs',
                'Endpoint' => $sqsQueue->arn(),
                'TopicArn' => $this->snsTopicArn,
                'Attributes' => [
                    'RawMessageDelivery' => 'true',
                ]
            ]);
        }
    }

    public static function messageBrokerConfigurationProvider(): array
    {
        return [
            ['array', 'array'],
            ['sqs', 'sqs'],
            ['sns', 'sqs'],
        ];
    }

    public static function messageBrokerConfigurationProviderForDeadLetterQueueTest(): array
    {
        //array implementation does not support DLQ
        return [
            ['sqs', 'sqs'],
            ['sns', 'sqs'],
        ];
    }

    private function givenThatMessageWasSent(): void
    {
        $message = new Message($this->channel, 'foo', uniqid());
        $this->messagePublisher->sendMessage($message);
        $this->sentMessages[] = $message;
    }

    private function givenThatRandomlyGeneratedMessagesWereSentInABatch(int $count): void
    {
        $messages = [];
        for ($i = 1; $i <= $count; $i++) {
            $messages[] = new Message($this->channel, 'foo_' . $i, uniqid());
        }

        $this->givenThatMessagesWereSentInABatch($messages);
    }

    private function givenThatMessagesWereSentInABatch(array $messages): void
    {
        try {
            $this->messagePublisher->batchSendMessages($messages);
            $this->sentMessages = array_merge($this->sentMessages, $messages);
        } catch (Exception $e) {
            $this->caughtException = $e;
        }
    }

    private function whenMessageIsReceivedWithDeletion(): void
    {
        $this->receivedMessages = [];
        $message = $this->messageSubscriber->getMessageFromChannel(
            $this->channel,
            true,
            null,
            $this->visibilityTimeout
        );

        if ($message !== null) {
            $this->receivedMessages[] = $message;
        }
    }

    private function whenMessageIsReceivedWithoutDeletion(): void
    {
        $this->receivedMessages = [];
        $message = $this->messageSubscriber->getMessageFromChannel(
            $this->channel,
            false,
            null,
            $this->visibilityTimeout
        );

        if ($message !== null) {
            $this->receivedMessages[] = $message;
        }
    }

    private function whenAllMessagesAreReceivedWithoutDeletion(): void
    {
        $this->receivedMessages = $this->messageSubscriber->getAllMessagesFromChannel($this->channel);
    }

    private function whenMessageIsReceivedFromDeadLetterQueue(): void
    {
        $this->receivedMessages = [];
        $channel = new Channel(name: $this->channelName . $this->dlqSuffix);
        $message = $this->messageSubscriber->getMessageFromChannel(
            $channel,
            false,
            null,
            $this->visibilityTimeout
        );
        if ($message !== null) {
            $this->receivedMessages[] = $message;
        }
    }

    private function whenReceivedMessageIsDeleted(bool $isInDlq = false): void
    {
        foreach ($this->receivedMessages as $receivedMessage) {
            $channelName = $receivedMessage->getChannelName();
            if ($isInDlq) {
                $channelName .= $this->dlqSuffix;
            }

            $this->messageSubscriber->deleteMessage($receivedMessage, new Channel($channelName));
        }
    }

    private function expectSentMessagesToMatchReceivedMessages(): void
    {
        foreach ($this->sentMessages as $sentMessage) {
            $receivedMessage = $this->getMessageByPayloadId($this->receivedMessages, $sentMessage->getPayloadId());
            if ($receivedMessage === null) {
                self::fail('Failed asserting that received messages match sent messages');
            }
            DomainAssert::assertMessagesAreEqual($sentMessage, $receivedMessage);
        }
    }

    private function expectNoMessageToBeReceived(): void
    {
        self::assertCount(0, $this->receivedMessages);
    }

    private function expectExceptionToBeCaught(string $class, string $message): void
    {
        self::assertInstanceOf($class, $this->caughtException);
        self::assertSame($message, $this->caughtException->getMessage());
        unset($this->caughtException);
    }

    private function cleanup(): void
    {
        if ($this->sqsClient !== null) {
            $result = $this->sqsClient->listQueues();

            foreach ($result->get('QueueUrls') ?? [] as $queueUrl) {
                $this->sqsClient->purgeQueue(['QueueUrl' => $queueUrl]);
                $this->sqsClient->deleteQueue(['QueueUrl' => $queueUrl]);
            }
        }

        if ($this->snsClient !== null && $this->snsTopicArn !== null) {
            $this->snsClient->deleteTopic(['TopicArn' => $this->snsTopicArn]);
        }
    }

    private function getMessageByPayloadId(array $messages, string $payloadId): ?Message
    {
        foreach ($messages as $message) {
            if ($message->getPayloadId() === $payloadId) {
                return $message;
            }
        }

        return null;
    }
}
