<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests;

use PHPUnit\Framework\Assert;
use Talentry\MessageBroker\Domain\Message\Message;

class DomainAssert extends Assert
{
    public static function assertMessagesAreEqual(Message $message1, Message $message2): void
    {
        self::assertSame($message1->getChannelName(), $message2->getChannelName());
        self::assertSame($message1->getPayloadId(), $message2->getPayloadId());
        self::assertSame($message1->getPayload(), $message2->getPayload());
    }
}
