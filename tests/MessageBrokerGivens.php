<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests;

use PHPUnit\Framework\Assert;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;

class MessageBrokerGivens
{
    private Channel $channel;
    private ?Message $message = null;

    public function __construct(
        private readonly MessagePublisher $messagePublisher,
        private readonly MessageSubscriber $messageSubscriber,
        private readonly MessageParser $messageParser,
    ) {
    }

    public function givenThatArbitraryMessageIsDispatched(): void
    {
        $this->channel = new Channel('foo');
        $message = new Message($this->channel, 'foo', uniqid());
        $this->messagePublisher->sendMessage($message);
    }

    public function whenMessageIsFetched(): void
    {
        $this->message = $this->messageSubscriber->getMessageFromChannel($this->channel);
    }

    public function expectNoTenantIdToBeParsed(): void
    {
        Assert::assertNull($this->messageParser->getTenantId($this->message));
    }

    public function expectUnknownMessageTypeToBeParsed(): void
    {
        Assert::assertSame('n/a', $this->messageParser->getMessageType($this->message));
    }

    public function expectNoKibanaLinkToBeParsed(): void
    {
        Assert::assertSame('n/a', $this->messageParser->getKibanaLink($this->message));
    }
}
