<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Domain\Channel;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Domain\Channel\RetentionPeriod;

class RetentionPeriodTest extends TestCase
{
    private int $minimumValueInDays = 2;
    private int $maximumValueInDays = 10;
    private RetentionPeriod $retentionPeriod;
    private int $retentionPeriodInt;

    public function testMinimumRetentionPeriod(): void
    {
        $this->givenMinimumRetentionPeriod();
        $this->whenExpressedAsInteger();
        $this->expectMinimumValue();
    }

    public function testMaximumRetentionPeriod(): void
    {
        $this->givenMaximumRetentionPeriod();
        $this->whenExpressedAsInteger();
        $this->expectMaximumValue();
    }

    public function testTwoDayRetentionPeriod(): void
    {
        $this->givenTwoDayRetentionPeriod();
        $this->whenExpressedAsInteger();
        $this->expectValueOfTwoDays();
    }

    public function testRetentionPeriodBelowMinimum(): void
    {
        $this->givenRetentionPeriodBelowMinimum();
        $this->whenExpressedAsInteger();
        $this->expectMinimumValue();
    }

    public function testRetentionPeriodAboveMaximum(): void
    {
        $this->givenRetentionPeriodAboveMaximum();
        $this->whenExpressedAsInteger();
        $this->expectMaximumValue();
    }

    private function givenMinimumRetentionPeriod(): void
    {
        $this->retentionPeriod = RetentionPeriod::minimum();
    }

    private function givenMaximumRetentionPeriod(): void
    {
        $this->retentionPeriod = RetentionPeriod::maximum();
    }

    private function givenTwoDayRetentionPeriod(): void
    {
        $this->retentionPeriod = RetentionPeriod::days(2);
    }

    private function givenRetentionPeriodBelowMinimum(): void
    {
        $this->retentionPeriod = RetentionPeriod::days($this->minimumValueInDays - 1);
    }

    private function givenRetentionPeriodAboveMaximum(): void
    {
        $this->retentionPeriod = RetentionPeriod::days($this->maximumValueInDays + 1);
    }

    private function whenExpressedAsInteger(): void
    {
        $this->retentionPeriodInt = $this->retentionPeriod->asInteger(
            $this->minimumValueInDays * 86400,
            $this->maximumValueInDays * 86400
        );
    }

    private function expectMinimumValue(): void
    {
        self::assertSame($this->minimumValueInDays * 86400, $this->retentionPeriodInt);
    }

    private function expectMaximumValue(): void
    {
        self::assertSame($this->maximumValueInDays * 86400, $this->retentionPeriodInt);
    }

    private function expectValueOfTwoDays(): void
    {
        self::assertSame(2 * 86400, $this->retentionPeriodInt);
    }
}
