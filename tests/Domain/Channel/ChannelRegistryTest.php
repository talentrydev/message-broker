<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Domain\Channel;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Serialization\Base64PhpSerializationStrategy;
use Talentry\MessageBroker\Infrastructure\Serialization\PhpSerializationStrategy;

class ChannelRegistryTest extends TestCase
{
    private ChannelRegistry $channelRegistry;
    private string $deadLetterQueueSuffix = '_dead_letter_queue';
    private Channel $channel1;
    private Channel $channel2;

    protected function setUp(): void
    {
        $this->channelRegistry = new ChannelRegistry($this->deadLetterQueueSuffix);
        $this->channel1 = new Channel('foo');
        $this->channel2 = new Channel('bar');
        $this->channelRegistry->registerChannel($this->channel1);
        $this->channelRegistry->registerChannel($this->channel2, new PhpSerializationStrategy());
    }

    public function testGetChannels(): void
    {
        $channels = $this->channelRegistry->getChannels();
        self::assertCount(2, $channels);
        self::assertSame($channels[0], $this->channel1);
        self::assertSame($channels[1], $this->channel2);
    }

    public function testGetAllDeadLetterQueueNames(): void
    {
        $dlqNames = $this->channelRegistry->getAllDeadLetterQueueNames();
        self::assertSame($dlqNames[0], $this->channel1->getName() . $this->deadLetterQueueSuffix);
        self::assertSame($dlqNames[1], $this->channel2->getName() . $this->deadLetterQueueSuffix);
    }

    public function testGetSerializationStrategy(): void
    {
        $strategy1 = $this->channelRegistry->getSerializationStrategy($this->channel1);
        self::assertSame(Base64PhpSerializationStrategy::class, $strategy1::class);
        $strategy2 = $this->channelRegistry->getSerializationStrategy($this->channel2);
        self::assertSame(PhpSerializationStrategy::class, $strategy2::class);
        $dlqChannel = new Channel($this->channelRegistry->getDeadLetterQueueNameForChannel($this->channel2));
        $dlqStrategy = $this->channelRegistry->getSerializationStrategy($dlqChannel);
        self::assertSame(PhpSerializationStrategy::class, $dlqStrategy::class);
    }

    public function testGetDeadLetterQueueNameForChannel(): void
    {
        $dlq = $this->channelRegistry->getDeadLetterQueueNameForChannel($this->channel1);
        self::assertSame($dlq, $this->channel1->getName() . $this->deadLetterQueueSuffix);
    }
}
