<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Domain\HealthCheck;

use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\VisibilityTimeout\VisibilityTimeout;
use Talentry\MessageBroker\ApplicationInterface\WaitTime\WaitTime;
use Talentry\MessageBroker\Domain\HealthCheck\MessageBrokerHealthChecker;
use Talentry\MessageBroker\Infrastructure\ArrayMessageBroker;

class MessageBrokerHealthCheckerTest extends TestCase
{
    public function testWithHealthyService(): void
    {
        $messagePublisher = $messageSubscriber = new ArrayMessageBroker();
        $healthCheck = new MessageBrokerHealthChecker($messagePublisher, $messageSubscriber);
        $report = $healthCheck->getHealthReport();

        self::assertSame('message-broker', $report->getServiceName());
        self::assertSame('array-array', $report->getDetails()['implementation']);
        self::assertTrue($report->isHealthy());
    }

    public function testWithUnhealthyService(): void
    {
        $broker = new class (new NullLogger()) extends ArrayMessageBroker
        {
            public function getMessageFromChannel(
                Channel $channel,
                bool $delete = true,
                ?WaitTime $waitTime = null,
                ?VisibilityTimeout $visibilityTimeout = null
            ): ?Message {
                throw new Exception();
            }
        };

        $healthCheck = new MessageBrokerHealthChecker($broker, $broker);
        $report = $healthCheck->getHealthReport();

        self::assertSame('message-broker', $report->getServiceName());
        self::assertFalse($report->isHealthy());
    }
}
