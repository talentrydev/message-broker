<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Presentation\Command;

use PHPUnit\Framework\TestCase;
use ColinODell\PsrTestLogger\TestLogger;
use Symfony\Component\Console\Tester\CommandTester;
use Talentry\MessageBroker\Application\Channel\ChannelFactory;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\Message\MessageFactory;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\InMemoryFailedMessageSpool;
use Talentry\MessageBroker\Presentation\Command\FailedMessageSpoolProcessorCommand;

class FailedMessageSpoolProcessorCommandTest extends TestCase
{
    private CommandTester $commandTester;
    private MessagePublisher $messagePublisher;
    private FailedMessageSpool $failedMessageSpool;
    private TestLogger $logger;
    private Message $message;
    private string $channelName = 'foo';
    private string $messageId = 'bar';

    protected function setUp(): void
    {
        $this->logger = new TestLogger();
        $this->failedMessageSpool = new InMemoryFailedMessageSpool();
        $this->messagePublisher = $this->createMock(MessagePublisher::class);
        $this->messagePublisher->method('getFailedMessageSpool')->willReturn($this->failedMessageSpool);

        $command = new FailedMessageSpoolProcessorCommand(
            $this->messagePublisher,
            $this->logger,
        );
        $this->commandTester = new CommandTester($command);

        $channelFactory = new ChannelFactory();
        $this->message = MessageFactory::generate(
            $channelFactory->createChannel($this->channelName),
            'payload',
            $this->messageId
        );
    }

    public function testCommand(): void
    {
        $this->messagePublisher->expects(self::once())->method('sendMessage')->with($this->message);

        $this->failedMessageSpool->push($this->message);
        $this->commandTester->execute([]);

        self::assertTrue($this->logger->hasInfoThatContains(
            "Message $this->channelName:$this->messageId pushed from failed message spool to message broker",
        ));
    }
}
