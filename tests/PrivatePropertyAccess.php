<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests;

use ReflectionClass;

class PrivatePropertyAccess
{
    public static function getPrivatePropertyValue(object $object, string $propertyName): mixed
    {
        $rc = new ReflectionClass($object);
        $prop = $rc->getProperty($propertyName);
        $prop->setAccessible(true);

        return $prop->getValue($object);
    }
}
