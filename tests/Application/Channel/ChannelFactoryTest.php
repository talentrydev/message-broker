<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Application\Channel;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Application\Channel\ChannelFactory;

class ChannelFactoryTest extends TestCase
{
    private ChannelFactory $channelFactory;

    protected function setUp(): void
    {
        $this->channelFactory = new ChannelFactory();
    }

    public function testCreateChannel(): void
    {
        $channel = $this->channelFactory->createChannel('foo');
        self::assertSame('foo', $channel->getName());
    }
}
