<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Mocks;

use Predis\ClientInterface;
use Predis\Command\CommandInterface;
use SplQueue;

class MockRedis implements ClientInterface
{
    /**
     * @var array<string,SplQueue>
     */
    private array $queues = [];

    public function getCommandFactory()
    {
    }

    public function getOptions()
    {
    }

    public function connect()
    {
    }

    public function disconnect()
    {
    }

    public function getConnection()
    {
    }

    public function createCommand($method, $arguments = array())
    {
    }

    public function executeCommand(CommandInterface $command)
    {
    }

    public function __call($method, $arguments)
    {
        $queueName = $arguments[0];
        $values = [];
        if (array_key_exists(1, $arguments)) {
            $values = $arguments[1];
        }

        if (!array_key_exists($queueName, $this->queues)) {
            $this->queues[$queueName] = new SplQueue();
        }

        switch ($method) {
            case 'rpush':
                foreach ($values as $value) {
                    $this->queues[$queueName]->push($value);
                }
                break;
            case 'lpop':
                if ($this->queues[$queueName]->isEmpty()) {
                    return null;
                }

                return $this->queues[$queueName]->shift();
        }
    }
}
