<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Mocks;

use PHPUnit\Framework\Assert;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;

class MessageGenerator
{
    private const int MESSAGE_MAX_SIZE = 180_000;

    public function __construct(
        private readonly ChannelRegistry $channelRegistry,
    ) {
    }

    public function generateMessage(string $payload = 'foo', Channel $channel = new Channel('foo')): Message
    {
        return new Message($channel, $payload, uniqid());
    }

    public function generateMessageAboveMaxSize(Channel $channel = new Channel('foo')): Message
    {
        $payloadSize = self::MESSAGE_MAX_SIZE + 1;
        $payload = str_repeat('.', $payloadSize);
        $message = $this->generateMessage($payload, $channel);
        $serializationStrategy = $this->channelRegistry->getSerializationStrategy($channel);
        Assert::assertSame($payloadSize, mb_strlen($serializationStrategy->serialize($message), '8bit'));

        return $message;
    }
}
