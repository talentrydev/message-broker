<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Mocks;

use Talentry\MessageBroker\ApplicationInterface\Message\Message as MessageInterface;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;

class MockMessageSerializationStrategy implements MessageSerializationStrategy
{
    public function serialize(MessageInterface $message): string
    {
        return $message->getPayload();
    }

    public function deserialize(string $data): MessageInterface
    {
        return new Message(new Channel('foo'), $data, uniqid());
    }
}
