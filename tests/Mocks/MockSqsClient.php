<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Mocks;

use Aws\Command;
use Aws\Result;
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;

class MockSqsClient extends SqsClient
{
    private const string BASE_QUEUE_URL = 'https://sqs.eu-central-1.amazonaws.com/test/';
    private const string BASE_QUEUE_ARN = 'arn:aws:sqs:eu-central-1:test:';

    private array $queues = [];
    private array $operations = [];
    private bool $tracing = true;
    private int $requestedWaitTime;
    private int $requestedVisibilityTimeout;

    public function __construct()
    {
    }

    public function createQueue(array $args = []): Result
    {
        if ($this->tracing) {
            $this->operations[] = 'createQueue';
        }
        $queueName = $args['QueueName'];
        $queueUrl = self::BASE_QUEUE_URL . $queueName;
        $args['Attributes']['QueueArn'] = self::BASE_QUEUE_ARN . $queueName;
        $args['Attributes']['QueueUrl'] = $queueUrl;
        $this->queues[$queueName] = $args;

        return new Result(['QueueUrl' => $queueUrl]);
    }

    public function getQueueUrl(array $args = []): Result
    {
        if ($this->tracing) {
            $this->operations[] = 'getQueueUrl';
        }

        $queueName = $args['QueueName'];

        if (!array_key_exists($queueName, $this->queues)) {
            $this->throwQueueDoesNotExistException();
        }

        return new Result(['QueueUrl' => $this->queues[$queueName]['Attributes']['QueueUrl']]);
    }

    public function getQueueAttributes(array $args = []): Result
    {
        if ($this->tracing) {
            $this->operations[] = 'getQueueAttributes';
        }

        $queueUrl = $args['QueueUrl'];

        return new Result($this->getQueueAttributesByUrl($queueUrl));
    }

    public function receiveMessage(array $args = []): Result
    {
        if ($this->tracing) {
            $this->operations[] = 'receiveMessage';
        }
        $this->requestedWaitTime = $args['WaitTimeSeconds'];
        $this->requestedVisibilityTimeout = $args['VisibilityTimeout'];

        return new Result(['Messages' => null]);
    }

    public function getOperations(): array
    {
        return $this->operations;
    }

    public function enableTracing(): void
    {
        $this->tracing = true;
    }

    public function disableTracing(): void
    {
        $this->tracing = false;
    }

    public function getRequestedWaitTime(): int
    {
        return $this->requestedWaitTime;
    }

    public function getRequestedVisibilityTimeout(): int
    {
        return $this->requestedVisibilityTimeout;
    }

    private function throwQueueDoesNotExistException(): void
    {
        throw new SqsException(
            'Queue does not exist',
            new Command('foo'),
            ['code' => 'AWS.SimpleQueueService.NonExistentQueue']
        );
    }

    private function getQueueAttributesByUrl(string $url): array
    {
        foreach ($this->queues as $queueName => $queue) {
            if ($queue['Attributes']['QueueUrl'] === $url) {
                return $queue;
            }
        }
        $this->throwQueueDoesNotExistException();
    }
}
