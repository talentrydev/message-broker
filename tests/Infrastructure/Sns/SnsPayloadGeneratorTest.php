<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sns;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Predis\ClientInterface;
use ColinODell\PsrTestLogger\TestLogger;
use Talentry\MessageBroker\ApplicationInterface\Message\MessageTooLargeException;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sns\SnsBatchPayload;
use Talentry\MessageBroker\Infrastructure\Sns\SnsPayload;
use Talentry\MessageBroker\Infrastructure\Sns\SnsPayloadGenerator;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageAttributesProvider;
use Talentry\MessageBroker\Tests\Mocks\MessageGenerator;
use Talentry\MessageBroker\Tests\Mocks\MockMessageSerializationStrategy;

class SnsPayloadGeneratorTest extends TestCase
{
    private const int MAX_PAYLOAD_SIZE = 180_000;

    private SnsPayloadGenerator $snsPayloadGenerator;
    private MessageSerializationStrategy $serializationStrategy;
    private ChannelRegistry $channelRegistry;
    private array $messageAttributes = ['foo' => 'bar'];
    private string $topicArn = 'arn:aws:sns:us-east-1:000000000000:foo';
    private Message $message;
    private Channel $channel;
    private array $messages = [];
    private array $validMessages = [];
    private SnsPayload $snsPayload;
    private TestLogger $testLogger;
    /**
     * @var SnsBatchPayload[]
     */
    private array $snsBatchPayloads = [];
    private MessageGenerator $messageGenerator;

    protected function setUp(): void
    {
        $this->testLogger = new TestLogger();
        $messageAttributesProvider = $this->createMock(SqsMessageAttributesProvider::class);
        $messageAttributesProvider->method('getMessageAttributes')->willReturn($this->messageAttributes);

        $this->channel = new Channel('foo');
        //we need a predictable serialization strategy for testing
        $this->serializationStrategy = new MockMessageSerializationStrategy();
        $this->channelRegistry = new ChannelRegistry();
        $this->channelRegistry->registerChannel(
            $this->channel,
            $this->serializationStrategy,
        );
        $this->messageGenerator = new MessageGenerator($this->channelRegistry);
        $this->snsPayloadGenerator = new SnsPayloadGenerator(
            $this->testLogger,
            $messageAttributesProvider,
            $this->channelRegistry,
            $this->createMock(ClientInterface::class),
        );
    }

    public function testGenerateSnsPayload(): void
    {
        $this->givenMessage();
        $this->whenSnsPayloadIsGenerated();
        $this->expectValidPayloadToBeGenerated();
    }

    public function testGenerateSnsPayloadWithVeryLargeMessage(): void
    {
        $this->givenMessageAboveMaxSize();
        $this->expectMessageTooLargeException();
        $this->whenSnsPayloadIsGenerated();
    }

    public function testGenerateSnsPayloads(): void
    {
        //this test is concerned with the payload structure, not the batching; that's why we only need one message
        $this->givenMultipleMessages(1);
        $this->whenSnsBatchPayloadsAreGenerated();
        $this->expectValidBatchPayloadsToBeGenerated();
    }

    public function testGenerateSnsPayloadsWithVeryLargeMessage(): void
    {
        // The valid messages should all still be sent, regardless of a bad message in the middle.
        $this->givenMultipleMessages(1);
        $this->givenMultipleMessagesAboveMaxSize(1);
        $this->givenMultipleMessages(1);
        $this->whenSnsBatchPayloadsAreGenerated();
        $this->expectMessageTooLargeLog();
        $this->expectValidBatchPayloadsToBeGenerated();
    }

    #[DataProvider('dataProviderForTestBatchingAccordingToNumberOfMessages')]
    public function testBatchingAccordingToNumberOfMessages(int $messageCount, int $batchCount): void
    {
        $this->givenMultipleMessages($messageCount);
        $this->whenSnsBatchPayloadsAreGenerated();
        $this->expectBatchCount($batchCount);
        $this->expectPayloadsToBeWithinLimits();
    }

    public static function dataProviderForTestBatchingAccordingToNumberOfMessages(): array
    {
        return [
            [10, 1],
            [15, 2],
            [20, 2],
            [21, 3],
            [100, 10],
            [101, 11],
        ];
    }

    #[DataProvider('dataProviderForTestBatchingAccordingToMessageSize')]
    public function testBatchingAccordingToMessageSize(
        int $messageCount,
        int $individualMessageSize,
        int $batchCount
    ): void {
        $this->givenMultipleMessagesOfSpecificSize($messageCount, $individualMessageSize);
        $this->whenSnsBatchPayloadsAreGenerated();
        $this->expectBatchCount($batchCount);
        $this->expectPayloadsToBeWithinLimits();
    }

    public static function dataProviderForTestBatchingAccordingToMessageSize(): array
    {
        return [
            [2, self::MAX_PAYLOAD_SIZE / 2, 1], //two messages add up exactly to max payload size
            [2, self::MAX_PAYLOAD_SIZE / 2 + 1, 2], //two messages add up to more than max payload size
            [3, self::MAX_PAYLOAD_SIZE / 2 + 1, 3],
        ];
    }

    private function givenMessage(): void
    {
        $this->message = $this->messageGenerator->generateMessage(channel: $this->channel);
    }

    private function givenMessageAboveMaxSize(): void
    {
        $this->message = $this->messageGenerator->generateMessageAboveMaxSize($this->channel);
    }

    private function givenMultipleMessages(int $messageCount): void
    {
        for ($i = 0; $i < $messageCount; $i++) {
            $message = $this->messageGenerator->generateMessage(channel: $this->channel);
            $this->messages[] = $message;
            $this->validMessages[] = $message;
        }
    }

    private function givenMultipleMessagesAboveMaxSize(int $messageCount): void
    {
        for ($i = 0; $i < $messageCount; $i++) {
            $this->messages[] = $this->messageGenerator->generateMessageAboveMaxSize($this->channel);
        }
    }

    private function givenMultipleMessagesOfSpecificSize(int $messageCount, int $messageSize): void
    {
        //we are interested in the size of the serialized message, not the raw payload
        $serializedMessage = str_repeat('x', $messageSize);
        self::assertSame($messageSize, mb_strlen($serializedMessage, '8bit'));
        $serializationStrategy = $this->createMock(MessageSerializationStrategy::class);
        $serializationStrategy
            ->expects(self::exactly($messageCount))
            ->method('serialize')
            ->willReturn($serializedMessage)
        ;

        $this->channelRegistry->registerChannel(
            $this->channel,
            $serializationStrategy,
        );

        for ($i = 0; $i < $messageCount; $i++) {
            $this->messages[] = new Message(new Channel('foo'), 'foo', uniqid());
        }
    }

    private function whenSnsPayloadIsGenerated(): void
    {
        $this->snsPayload = $this->snsPayloadGenerator->generateSnsPayload(
            $this->message,
            $this->topicArn,
        );
    }

    private function whenSnsBatchPayloadsAreGenerated(): void
    {
        $generator = $this->snsPayloadGenerator->generateSnsBatchPayloads(
            $this->messages,
            $this->topicArn,
        );

        foreach ($generator as $payload) {
            $this->snsBatchPayloads = array_merge($this->snsBatchPayloads, $payload);
        }
    }

    private function expectValidPayloadToBeGenerated(): void
    {
        $expectedPayload = [
            'Message' => $this->serializationStrategy->serialize($this->message),
            'MessageAttributes' => $this->messageAttributes,
            'TopicArn' => $this->topicArn
        ];

        self::assertSame($expectedPayload, $this->snsPayload->toArray());
    }

    private function expectValidBatchPayloadsToBeGenerated(): void
    {
        $entries = [];
        foreach ($this->validMessages as $message) {
            $entries[] = [
                'Id' => $message->getId(),
                'Message' => $this->serializationStrategy->serialize($message),
                'MessageAttributes' => $this->messageAttributes,
            ];
        }

        $expectedPayloads = [
            [
                'PublishBatchRequestEntries' => $entries,
                'TopicArn' => $this->topicArn,
            ]
        ];

        $actualPayloads = [];
        foreach ($this->snsBatchPayloads as $snsBatchPayload) {
            $actualPayloads[] = $snsBatchPayload->toArray();
        }

        self::assertSame($expectedPayloads, $actualPayloads);
    }

    private function expectBatchCount(int $count): void
    {
        self::assertCount($count, $this->snsBatchPayloads);
    }

    private function expectMessageTooLargeException(): void
    {
        self::expectException(MessageTooLargeException::class);
    }

    private function expectMessageTooLargeLog(): void
    {
        $this->testLogger->hasRecord('SNS message too large. Discarding.', 'error');
    }

    private function expectPayloadsToBeWithinLimits(): void
    {
        foreach ($this->snsBatchPayloads as $payload) {
            $payloadSize = 0;
            foreach ($payload->toArray()['PublishBatchRequestEntries'] as $entry) {
                $payloadSize += mb_strlen($entry['Message'], '8bit');
            }

            self::assertLessThanOrEqual(180_000, $payloadSize);
        }
    }
}
