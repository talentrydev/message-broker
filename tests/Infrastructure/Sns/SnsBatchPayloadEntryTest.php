<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sns;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Infrastructure\Sns\SnsBatchPayloadEntry;

class SnsBatchPayloadEntryTest extends TestCase
{
    public function testToArray(): void
    {
        $entry = new SnsBatchPayloadEntry('id', 'message', ['foo' => 'bar']);
        $expectedArray = [
            'Id' => 'id',
            'Message' => 'message',
            'MessageAttributes' => ['foo' => 'bar'],
        ];
        self::assertSame($expectedArray, $entry->toArray());
    }
}
