<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sns;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Infrastructure\Sns\SnsPayload;

class SnsPayloadTest extends TestCase
{
    public function testToArray(): void
    {
        $payload = new SnsPayload('message', ['foo' => 'bar'], 'topic');
        $expectedArray = [
            'Message' => 'message',
            'MessageAttributes' => ['foo' => 'bar'],
            'TopicArn' => 'topic',
        ];
        self::assertSame($expectedArray, $payload->toArray());
    }

    public function testSize(): void
    {
        $payload = new SnsPayload('message', ['foo' => 'bar'], 'topic');
        self::assertSame(7, $payload->size()); //'message' contains 7 bytes
    }
}
