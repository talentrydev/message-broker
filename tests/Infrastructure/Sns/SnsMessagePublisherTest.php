<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sns;

use Aws\Command;
use Aws\Result;
use Aws\Sns\Exception\SnsException;
use Aws\Sns\SnsClient;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Promise\RejectedPromise;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Predis\ClientInterface;
use Psr\Log\LoggerInterface;
use ColinODell\PsrTestLogger\TestLogger;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\MessageTooLargeException;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\InMemoryFailedMessageSpool;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sns\SnsMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Sns\SnsPayloadGenerator;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageAttributesProvider;
use Talentry\MessageBroker\Tests\Infrastructure\FailedMessageSpool\FailedMessageSpoolTester;
use Talentry\MessageBroker\Tests\Infrastructure\Logging\LoggerTraitTester;
use Talentry\MessageBroker\Tests\Mocks\MessageGenerator;
use Talentry\MessageBroker\Tests\Mocks\MockMessageSerializationStrategy;

class SnsMessagePublisherTest extends TestCase
{
    use LoggerTraitTester;
    use FailedMessageSpoolTester;

    private SnsMessagePublisher $snsMessagePublisher;
    private SnsClient|MockObject $snsClient;
    private MessageSerializationStrategy $messageSerializationStrategy;
    private ChannelRegistry $channelRegistry;
    private TestLogger $logger;
    private InMemoryFailedMessageSpool $failedMessageSpool;
    private MessageGenerator $messageGenerator;
    private Channel $channel;
    private string $snsTopicArn = 'arn:sns:foo';
    /**
     * @var Message[]
     */
    private array $messages;
    private string $tenantId = '1';
    private string $messageType = 'foo';
    private string $publishError = 'simulated error';

    protected function setUp(): void
    {
        $this->snsClient = $this->createMock(SnsClient::class);
        $messageParser = $this->createMock(MessageParser::class);
        $messageParser->method('getTenantId')->willReturn($this->tenantId);
        $messageParser->method('getMessageType')->willReturn($this->messageType);
        $this->logger = new TestLogger();
        $this->failedMessageSpool = new InMemoryFailedMessageSpool();

        $this->channel = new Channel('foo');
        $this->messageSerializationStrategy = new MockMessageSerializationStrategy();
        $this->channelRegistry = new ChannelRegistry();
        $this->channelRegistry->registerChannel(
            $this->channel,
            $this->messageSerializationStrategy,
        );

        $snsPayloadGenerator = new SnsPayloadGenerator(
            new TestLogger(),
            new SqsMessageAttributesProvider($messageParser),
            $this->channelRegistry,
            $this->createMock(ClientInterface::class),
        );

        $this->snsMessagePublisher = new SnsMessagePublisher(
            $this->snsClient,
            $this->failedMessageSpool,
            $snsPayloadGenerator,
            $this->logger,
            $this->snsTopicArn,
        );

        $this->messageGenerator = new MessageGenerator($this->channelRegistry);

        for ($i = 0; $i < 30; $i++) {
            $this->messages[] = $this->messageGenerator->generateMessage('payload_' . $i, $this->channel);
        }
    }

    public function testSendMessage(): void
    {
        $message = $this->messages[0];
        $snsPayload = $this->toSnsPayload($message);
        $snsPayload['TopicArn'] = $this->snsTopicArn;

        $this->snsClient
            ->expects(self::once())
            ->method('__call')
            ->with('publish', [$snsPayload])
        ;

        $this->snsMessagePublisher->sendMessage($message);

        $this->assertMessageWasLogged($message);
    }

    public function testSendMessageThatGetsRejected(): void
    {
        $message = $this->messages[0];
        $snsPayload = $this->toSnsPayload($message);
        $snsPayload['TopicArn'] = $this->snsTopicArn;

        $this->snsClient
            ->expects(self::once())
            ->method('__call')
            ->with('publish', [$snsPayload])
            ->willThrowException(new SnsException($this->publishError, $this->createMock(Command::class)))
        ;

        $this->snsMessagePublisher->sendMessage($message);
        $this->assertMessageWasSpooled($message);
        $this->assertSpooledMessageWasLogged($message, $this->publishError);
    }

    public function testSendMessageThatIsTooLarge(): void
    {
        $this->expectException(MessageTooLargeException::class);
        $message = $this->messageGenerator->generateMessageAboveMaxSize($this->channel);
        $this->snsMessagePublisher->sendMessage($message);
        $this->assertMessageWasNotSpooled($message);
    }

    public function testBatchSendMessages(): void
    {
        $firstBatch = array_slice($this->messages, 0, 10);
        $secondBatch = array_slice($this->messages, 10, 10);
        $thirdBatch = array_slice($this->messages, 20, 10);

        //we will simulate a failure for half of the messages in each of the first two batches
        $firstBatchSuccesses = array_slice($firstBatch, 0, 5);
        $firstBatchFailures = array_slice($firstBatch, 5, 5);
        $secondBatchSuccesses = array_slice($secondBatch, 0, 5);
        $secondBatchFailures = array_slice($secondBatch, 5, 5);

        $this->snsClient
            ->expects(self::exactly(3))
            ->method('getCommand')
            ->willReturnCallback(fn (string $commandName, array $args) => new Command($commandName, $args));


        $failedCommandMock = $this->createMock(Command::class);
        $failedCommandMock->method('toArray')
            ->willReturn(
                [
                    'MessageIds' => array_map(
                        fn(Message $message) => $message->getId(),
                        array_merge($firstBatchFailures, $secondBatchFailures, $thirdBatch)
                    )
                ]
            );

        $this->snsClient
            ->expects(self::exactly(3))
            ->method('executeAsync')
            ->willReturnOnConsecutiveCalls(
                //here we simulate a failure for half of the messages in first two batches
                $this->createFulfilledPromise($firstBatchSuccesses, $firstBatchFailures),
                $this->createFulfilledPromise($secondBatchSuccesses, $secondBatchFailures),
                //in the third batch we simulate a rejected promise
                new RejectedPromise(new SnsException($this->publishError, $failedCommandMock))
            )
        ;

        $this->snsMessagePublisher->batchSendMessages($this->messages);

        //expect successfully sent messages to be logged
        foreach (array_merge($firstBatchSuccesses, $secondBatchSuccesses) as $message) {
            $this->assertMessageWasLogged($message);
        }

        //expect failed messages to be spooled
        foreach (array_merge($firstBatchFailures, $secondBatchFailures, $thirdBatch) as $message) {
            $this->assertMessageWasSpooled($message);
            $this->assertSpooledMessageWasLogged($message, $this->publishError);
        }
    }

    public function testBatchSendMessageThatIsTooLarge(): void
    {
        $message = $this->messageGenerator->generateMessageAboveMaxSize($this->channel);
        $this->snsMessagePublisher->batchSendMessages([$message], $this->messageSerializationStrategy);
        $this->assertMessageWasNotSpooled($message);
    }

    /**
     * @return array<string,mixed>
     */
    private function toSnsPayload(Message $message, bool $includeId = false): array
    {
        $array = $includeId ? ['Id' => $message->getId()] : [];
        return $array + [
            'Message' => $this->messageSerializationStrategy->serialize($message),
            'MessageAttributes' => [
                'MessageId' => [
                    'DataType' => 'String',
                    'StringValue' => $message->getId()
                ],
                'TenantId' => [
                    'DataType' => 'String',
                    'StringValue' => $this->tenantId,
                ],
                'MessageType' => [
                    'DataType' => 'String',
                    'StringValue' => $this->messageType,
                ],
                'Channel' => [
                    'DataType' => 'String',
                    'StringValue' => $message->getChannel()->getName(),
                ]
            ],
        ];
    }

    private function toSnsPayloads(array $messages, bool $includeId = false): array
    {
        return array_map(fn (Message $message) => $this->toSnsPayload($message, includeId: $includeId), $messages);
    }

    private function createFulfilledPromise(array $successfulMessages, array $failedMessages): FulfilledPromise
    {
        return new FulfilledPromise(new Result([
            'Successful' => array_map(
                fn (Message $message) => [
                    'Id' => $message->getId(),
                    'MessageId' => sprintf('%s_%s', $message->getChannelName(), $message->getId()),
                ],
                $successfulMessages
            ),
            'Failed' => array_map(
                fn (Message $message) => [
                    'Id' => $message->getId(),
                    'MessageId' => sprintf('%s_%s', $message->getChannelName(), $message->getId()),
                    'Code' => $this->publishError,
                ],
                $failedMessages
            ),
        ]));
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    protected function getFailedMessageSpool(): FailedMessageSpool
    {
        return $this->failedMessageSpool;
    }
}
