<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sns;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Infrastructure\Sns\SnsBatchPayload;
use Talentry\MessageBroker\Infrastructure\Sns\SnsBatchPayloadEntry;

class SnsBatchPayloadTest extends TestCase
{
    private string $topicArn = 'arn:sns:foo';
    private string $messageId = 'messageId';
    private string $message = 'message';
    private array $attributes = ['foo' => 'bar'];
    private SnsBatchPayloadEntry $entry;

    protected function setUp(): void
    {
        $this->entry = new SnsBatchPayloadEntry($this->messageId, $this->message, $this->attributes);
    }

    public function testToArray(): void
    {
        $batchPayload = new SnsBatchPayload($this->topicArn, [$this->entry]);
        $expectedArray = [
            'PublishBatchRequestEntries' => [[
                'Id' => $this->messageId,
                'Message' => $this->message,
                'MessageAttributes' => $this->attributes,
            ]],
            'TopicArn' => $this->topicArn,
        ];
        self::assertSame($expectedArray, $batchPayload->toArray());
    }

    public function testGetMessageIds(): void
    {
        $batchPayload = new SnsBatchPayload($this->topicArn, [$this->entry]);
        self::assertSame([$this->messageId], $batchPayload->getMessageIds());
    }

    public function testWithEntry(): void
    {
        $batchPayload = new SnsBatchPayload($this->topicArn);
        $modifiedBatchPayload = $batchPayload->withEntry($this->entry);

        //test immutability
        $expectedArray = [
            'PublishBatchRequestEntries' => [],
            'TopicArn' => $this->topicArn,
        ];
        self::assertSame($expectedArray, $batchPayload->toArray());

        $expectedArray = [
            'PublishBatchRequestEntries' => [[
                'Id' => $this->messageId,
                'Message' => $this->message,
                'MessageAttributes' => $this->attributes,
            ]],
            'TopicArn' => $this->topicArn,
        ];
        self::assertSame($expectedArray, $modifiedBatchPayload->toArray());
    }

    public function testSize(): void
    {
        $batchPayload = new SnsBatchPayload($this->topicArn, [$this->entry, $this->entry]);
        self::assertSame(14, $batchPayload->size()); //two messages of 7 bytes each
    }
}
