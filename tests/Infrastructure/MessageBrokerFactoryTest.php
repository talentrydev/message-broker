<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure;

use Aws\Sns\SnsClient;
use Aws\Sqs\SqsClient;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\Infrastructure\ArrayMessageBroker;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaFactory;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaMessageSubscriber;
use Talentry\MessageBroker\Infrastructure\MessageBrokerFactory;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sns\SnsMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Sns\SnsPayloadGenerator;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageAttributesProvider;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageSubscriber;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueueFactory;
use Talentry\MessageBroker\Tests\PrivatePropertyAccess;
use RuntimeException;

class MessageBrokerFactoryTest extends TestCase
{
    private FailedMessageSpool $failedMessageSpool;
    private SnsPayloadGenerator $snsPayloadGenerator;
    private SqsMessageAttributesProvider $sqsMessageAttributesProvider;
    private ChannelRegistry $channelRegistry;
    private LoggerInterface $logger;
    private CacheItemPoolInterface $sqsQueuesCache;

    protected function setUp(): void
    {
        $this->failedMessageSpool = $this->createMock(FailedMessageSpool::class);
        $this->snsPayloadGenerator = $this->createMock(SnsPayloadGenerator::class);
        $this->sqsMessageAttributesProvider = $this->createMock(SqsMessageAttributesProvider::class);
        $this->channelRegistry = $this->createMock(ChannelRegistry::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->sqsQueuesCache = $this->createMock(CacheItemPoolInterface::class);
    }

    #[DataProvider('snsDataProvider')]
    public function testGenerateSnsMessagePublisher(
        ?string $snsTopicArn,
        ?string $snsEndpoint,
        ?string $awsRegion,
    ): void {
        if ($snsTopicArn === null) {
            $this->expectException(RuntimeException::class);
            $this->expectExceptionMessage('SNS topic ARN must be provided when using SNS publisher implementation');
        }

        $factory = new MessageBrokerFactory(
            failedMessageSpool: $this->failedMessageSpool,
            snsPayloadGenerator: $this->snsPayloadGenerator,
            sqsMessageAttributesProvider: $this->sqsMessageAttributesProvider,
            channelRegistry: $this->channelRegistry,
            logger: $this->logger,
            publisherImplementation: 'sns',
            awsRegion: $awsRegion,
            snsTopicArn: $snsTopicArn,
            snsEndpoint: $snsEndpoint,
        );

        $publisher = $factory->generateMessagePublisher();
        self::assertInstanceOf(SnsMessagePublisher::class, $publisher);

        /** @var SnsClient $snsClient */
        $snsClient = PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'snsClient');
        if ($snsEndpoint !== null) {
            self::assertSame($snsEndpoint, (string) $snsClient->getEndpoint());
        }
        self::assertSame($snsClient->getRegion(), $awsRegion ?? 'eu-central-1');

        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'failedMessageSpool'),
            $this->failedMessageSpool,
        );

        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'snsPayloadGenerator'),
            $this->snsPayloadGenerator,
        );

        self::assertSame(PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'logger'), $this->logger);

        self::assertSame($snsTopicArn, PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'snsTopicArn'));
    }

    public static function snsDataProvider(): array
    {
        return [
            [null, null, null],
            ['arn:aws:sns:eu-central-1:000000000000:foo', null, null],
            ['arn:aws:sns:eu-central-1:000000000000:foo', 'https://localstack', null],
            ['arn:aws:sns:eu-central-1:000000000000:foo', 'https://localstack', 'us-east-1'],
        ];
    }

    #[DataProvider('sqsDataProvider')]
    public function testGenerateSqsMessagePublisher(?string $namespace, ?string $sqsEndpoint, ?string $awsRegion): void
    {
        $factory = new MessageBrokerFactory(
            failedMessageSpool: $this->failedMessageSpool,
            snsPayloadGenerator: $this->snsPayloadGenerator,
            sqsMessageAttributesProvider: $this->sqsMessageAttributesProvider,
            channelRegistry: $this->channelRegistry,
            logger: $this->logger,
            publisherImplementation: 'sqs',
            namespace: $namespace,
            awsRegion: $awsRegion,
            sqsEndpoint: $sqsEndpoint,
            sqsQueuesCache: $this->sqsQueuesCache,
        );

        $publisher = $factory->generateMessagePublisher();
        self::assertInstanceOf(SqsMessagePublisher::class, $publisher);

        /** @var SqsClient $sqsClient */
        $sqsClient = PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'sqsClient');
        if ($sqsEndpoint !== null) {
            self::assertSame($sqsEndpoint, (string) $sqsClient->getEndpoint());
        }

        self::assertSame($sqsClient->getRegion(), $awsRegion ?? 'eu-central-1');

        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'sqsMessageAttributesProvider'),
            $this->sqsMessageAttributesProvider,
        );

        /** @var SqsQueueFactory $sqsQueueFactory */
        $sqsQueueFactory = PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'sqsQueueFactory');
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($sqsQueueFactory, 'sqsQueuesCache'),
            $this->sqsQueuesCache,
        );

        self::assertSame($namespace, PrivatePropertyAccess::getPrivatePropertyValue($sqsQueueFactory, 'namespace'));

        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'failedMessageSpool'),
            $this->failedMessageSpool,
        );

        self::assertSame(PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'logger'), $this->logger);

        self::assertSame(
            $this->channelRegistry,
            PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'channelRegistry'),
        );
    }

    #[DataProvider('sqsDataProvider')]
    public function testGenerateSqsMessageSubscriber(?string $namespace, ?string $sqsEndpoint, ?string $awsRegion): void
    {
        $factory = new MessageBrokerFactory(
            failedMessageSpool: $this->failedMessageSpool,
            snsPayloadGenerator: $this->snsPayloadGenerator,
            sqsMessageAttributesProvider: $this->sqsMessageAttributesProvider,
            channelRegistry: $this->channelRegistry,
            logger: $this->logger,
            subscriberImplementation: 'sqs',
            namespace: $namespace,
            awsRegion: $awsRegion,
            sqsEndpoint: $sqsEndpoint,
            sqsQueuesCache: $this->sqsQueuesCache,
        );

        $subscriber = $factory->generateMessageSubscriber();
        self::assertInstanceOf(SqsMessageSubscriber::class, $subscriber);

        /** @var SqsClient $sqsClient */
        $sqsClient = PrivatePropertyAccess::getPrivatePropertyValue($subscriber, 'sqsClient');
        if ($sqsEndpoint !== null) {
            self::assertSame($sqsEndpoint, (string) $sqsClient->getEndpoint());
        }

        self::assertSame($sqsClient->getRegion(), $awsRegion ?? 'eu-central-1');

        /** @var SqsQueueFactory $sqsQueueFactory */
        $sqsQueueFactory = PrivatePropertyAccess::getPrivatePropertyValue($subscriber, 'sqsQueueFactory');
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($sqsQueueFactory, 'sqsQueuesCache'),
            $this->sqsQueuesCache,
        );

        self::assertSame($namespace, PrivatePropertyAccess::getPrivatePropertyValue($sqsQueueFactory, 'namespace'));

        self::assertSame(PrivatePropertyAccess::getPrivatePropertyValue($subscriber, 'logger'), $this->logger);

        self::assertSame(
            $this->channelRegistry,
            PrivatePropertyAccess::getPrivatePropertyValue($subscriber, 'channelRegistry'),
        );
    }

    public static function sqsDataProvider(): array
    {
        return [
            [null, null, null],
            ['prod', null, null],
            ['prod', 'https://localstack', null],
            ['prod', 'https://localstack', 'us-east-1'],
        ];
    }

    #[DataProvider('kafkaDataProvider')]
    public function testGenerateKafkaMessagePublisher(
        ?string $kafkaBrokerList,
        ?string $kafkaAutoOffsetReset,
        ?string $kafkaEnablePartitionEof,
        ?string $kafkaEnableAutoCommit,
        ?int $kafkaMaxPollIntervalMs,
        ?int $kafkaSessionTimeoutMs,
        ?int $kafkaLogLevel,
    ): void {
        if ($kafkaBrokerList === null) {
            $this->expectException(RuntimeException::class);
            $this->expectExceptionMessage(
                'Kafka broker list must be provided when using kafka publisher implementation',
            );
        }

        $factory = new MessageBrokerFactory(
            failedMessageSpool: $this->failedMessageSpool,
            snsPayloadGenerator: $this->snsPayloadGenerator,
            sqsMessageAttributesProvider: $this->sqsMessageAttributesProvider,
            channelRegistry: $this->channelRegistry,
            logger: $this->logger,
            publisherImplementation: 'kafka',
            kafkaBrokerList: $kafkaBrokerList,
            kafkaAutoOffsetReset: $kafkaAutoOffsetReset,
            kafkaEnablePartitionEof: $kafkaEnablePartitionEof,
            kafkaEnableAutoCommit: $kafkaEnableAutoCommit,
            kafkaLogLevel: $kafkaLogLevel,
            kafkaMaxPollIntervalMs: $kafkaMaxPollIntervalMs,
            kafkaSessionTimeoutMs: $kafkaSessionTimeoutMs,
        );

        $publisher = $factory->generateMessagePublisher();
        self::assertInstanceOf(KafkaMessagePublisher::class, $publisher);

        $this->assertKafkaFactoryIsProperlyConfigured(
            PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'kafkaFactory'),
            $kafkaBrokerList,
            $kafkaAutoOffsetReset,
            $kafkaEnablePartitionEof,
            $kafkaEnableAutoCommit,
            $kafkaMaxPollIntervalMs,
            $kafkaSessionTimeoutMs,
            $kafkaLogLevel,
        );

        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'channelRegistry'),
            $this->channelRegistry,
        );

        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'failedMessageSpool'),
            $this->failedMessageSpool,
        );

        self::assertSame(PrivatePropertyAccess::getPrivatePropertyValue($publisher, 'logger'), $this->logger);
    }

    #[DataProvider('kafkaDataProvider')]
    public function testGenerateKafkaMessageSubscriber(
        ?string $kafkaBrokerList,
        ?string $kafkaAutoOffsetReset,
        ?string $kafkaEnablePartitionEof,
        ?string $kafkaEnableAutoCommit,
        ?int $kafkaMaxPollIntervalMs,
        ?int $kafkaSessionTimeoutMs,
        ?int $kafkaLogLevel,
    ): void {
        if ($kafkaBrokerList === null) {
            $this->expectException(RuntimeException::class);
            $this->expectExceptionMessage(
                'Kafka broker list must be provided when using kafka subscriber implementation',
            );
        }

        $factory = new MessageBrokerFactory(
            failedMessageSpool: $this->failedMessageSpool,
            snsPayloadGenerator: $this->snsPayloadGenerator,
            sqsMessageAttributesProvider: $this->sqsMessageAttributesProvider,
            channelRegistry: $this->channelRegistry,
            logger: $this->logger,
            subscriberImplementation: 'kafka',
            kafkaBrokerList: $kafkaBrokerList,
            kafkaAutoOffsetReset: $kafkaAutoOffsetReset,
            kafkaEnablePartitionEof: $kafkaEnablePartitionEof,
            kafkaEnableAutoCommit: $kafkaEnableAutoCommit,
            kafkaLogLevel: $kafkaLogLevel,
            kafkaMaxPollIntervalMs: $kafkaMaxPollIntervalMs,
            kafkaSessionTimeoutMs: $kafkaSessionTimeoutMs,
        );

        $subscriber = $factory->generateMessageSubscriber();
        self::assertInstanceOf(KafkaMessageSubscriber::class, $subscriber);

        /** @var KafkaFactory $kafkaFactory */
        $kafkaFactory = PrivatePropertyAccess::getPrivatePropertyValue($subscriber, 'kafkaFactory');
        $this->assertKafkaFactoryIsProperlyConfigured(
            $kafkaFactory,
            $kafkaBrokerList,
            $kafkaAutoOffsetReset,
            $kafkaEnablePartitionEof,
            $kafkaEnableAutoCommit,
            $kafkaMaxPollIntervalMs,
            $kafkaSessionTimeoutMs,
            $kafkaLogLevel,
        );

        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($subscriber, 'channelRegistry'),
            $this->channelRegistry,
        );
    }

    public static function kafkaDataProvider(): array
    {
        return [
            [null, null, null, null, null, null, null],
            ['kafka:9092', null, null, null, null, null, null],
            ['kafka:9092', 'latest', null, null, null, null, null],
            ['kafka:9092', 'latest', 'false', null, null, null, null],
            ['kafka:9092', 'latest', 'false', 'true', null, null, null],
            ['kafka:9092', 'latest', 'false', 'true', 300, null, null],
            ['kafka:9092', 'latest', 'false', 'true', 300, 400, null],
            ['kafka:9092', 'latest', 'false', 'true', 300, 400, LOG_DEBUG],
        ];
    }

    public function testGenerateArrayMessagePublisher(): void
    {
        $factory = new MessageBrokerFactory(
            failedMessageSpool: $this->failedMessageSpool,
            snsPayloadGenerator: $this->snsPayloadGenerator,
            sqsMessageAttributesProvider: $this->sqsMessageAttributesProvider,
            channelRegistry: $this->channelRegistry,
            logger: $this->logger,
            publisherImplementation: 'array',
        );

        $publisher = $factory->generateMessagePublisher();
        self::assertInstanceOf(ArrayMessageBroker::class, $publisher);
    }

    public function testGenerateArrayMessageSubscriber(): void
    {
        $factory = new MessageBrokerFactory(
            failedMessageSpool: $this->failedMessageSpool,
            snsPayloadGenerator: $this->snsPayloadGenerator,
            sqsMessageAttributesProvider: $this->sqsMessageAttributesProvider,
            channelRegistry: $this->channelRegistry,
            logger: $this->logger,
            subscriberImplementation: 'array',
        );

        $subscriber = $factory->generateMessageSubscriber();
        self::assertInstanceOf(ArrayMessageBroker::class, $subscriber);
    }

    private function assertKafkaFactoryIsProperlyConfigured(
        KafkaFactory $kafkaFactory,
        ?string $kafkaBrokerList,
        ?string $kafkaAutoOffsetReset,
        ?string $kafkaEnablePartitionEof,
        ?string $kafkaEnableAutoCommit,
        ?int $kafkaMaxPollIntervalMs,
        ?int $kafkaSessionTimeoutMs,
        ?int $kafkaLogLevel,
    ): void {
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'brokerList'),
            $kafkaBrokerList,
        );
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'autoOffsetReset'),
            $kafkaAutoOffsetReset ?? 'earliest',
        );
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'enablePartitionEof'),
            $kafkaEnablePartitionEof ?? 'true',
        );
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'enableAutoCommit'),
            $kafkaEnableAutoCommit ?? 'false',
        );
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'maxPollIntervalMs'),
            $kafkaMaxPollIntervalMs ?? 300_000,
        );
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'sessionTimeoutMs'),
            $kafkaSessionTimeoutMs ?? 45_000,
        );
        self::assertSame(
            PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'logLevel'),
            $kafkaLogLevel ?? LOG_ERR,
        );
    }
}
