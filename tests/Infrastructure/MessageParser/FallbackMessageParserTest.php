<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\MessageParser;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Infrastructure\ArrayMessageBroker;
use Talentry\MessageBroker\Infrastructure\MessageParser\FallbackMessageParser;
use Talentry\MessageBroker\Tests\MessageBrokerGivens;

class FallbackMessageParserTest extends TestCase
{
    private MessageBrokerGivens $givens;

    protected function setUp(): void
    {
        $broker = new ArrayMessageBroker();
        $this->givens = new MessageBrokerGivens(
            $broker,
            $broker,
            new FallbackMessageParser(),
        );
    }

    public function testParsingWithArbitraryType(): void
    {
        $this->givens->givenThatArbitraryMessageIsDispatched();
        $this->givens->whenMessageIsFetched();
        $this->givens->expectNoTenantIdToBeParsed();
        $this->givens->expectUnknownMessageTypeToBeParsed();
        $this->givens->expectNoKibanaLinkToBeParsed();
    }
}
