<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Serialization;

use PHPUnit\Framework\TestCase;
use RuntimeException;
use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\ErrorHandler;
use Talentry\ErrorHandling\Factory\ErrorHandlerFactory;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\Serialization\PhpSerializationStrategy;

class PhpSerializationStrategyTest extends TestCase
{
    private PhpSerializationStrategy $serializationStrategy;
    private Message $message;
    private ErrorHandler $errorHandler;

    protected function setUp(): void
    {
        $this->serializationStrategy = new PhpSerializationStrategy();
        $this->message = new Message(new Channel('foo'), 'foo', uniqid());
        $this->errorHandler = (new ErrorHandlerFactory())->generate();
        $this->errorHandler->startHandling(Severity::WARNING);
    }

    protected function tearDown(): void
    {
        $this->errorHandler->stopHandling();
    }

    public function testDeserializePhpSerializedMessage(): void
    {
        $serialized = $this->serializationStrategy->serialize($this->message);
        $unserialized = $this->serializationStrategy->deserialize($serialized);
        self::assertEquals($this->message, $unserialized);
    }

    public function testDeserializePhpSerializedRandomObject(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Failed to unserialize data');
        $serialized = serialize((object) ['foo' => 'bar']);
        $this->serializationStrategy->deserialize($serialized);
    }

    public function testDeserializeOrdinaryString(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Failed to unserialize data');
        $serialized = 'foo';
        $this->serializationStrategy->deserialize($serialized);
    }
}
