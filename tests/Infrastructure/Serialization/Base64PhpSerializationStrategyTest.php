<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Serialization;

use PHPUnit\Framework\TestCase;
use RuntimeException;
use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\ErrorHandler;
use Talentry\ErrorHandling\Factory\ErrorHandlerFactory;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\Serialization\Base64PhpSerializationStrategy;
use Talentry\MessageBroker\Infrastructure\Serialization\PhpSerializationStrategy;

class Base64PhpSerializationStrategyTest extends TestCase
{
    private Base64PhpSerializationStrategy $serializationStrategy;
    private Message $message;
    private ErrorHandler $errorHandler;

    protected function setUp(): void
    {
        $this->serializationStrategy = new Base64PhpSerializationStrategy();
        $this->message = new Message(new Channel('foo'), 'foo', uniqid());
        $this->errorHandler = (new ErrorHandlerFactory())->generate();
        $this->errorHandler->startHandling(Severity::WARNING);
    }

    protected function tearDown(): void
    {
        $this->errorHandler->stopHandling();
    }

    public function testDeserializeBase64PhpSerializedMessage(): void
    {
        $serialized = $this->serializationStrategy->serialize($this->message);
        $unserialized = $this->serializationStrategy->deserialize($serialized);
        self::assertEquals($this->message, $unserialized);
    }

    public function testDeserializePhpSerializedMessage(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Failed to base64 decode data');
        $serialized = (new PhpSerializationStrategy())->serialize($this->message);
        $this->serializationStrategy->deserialize($serialized);
    }

    public function testDeserializeOrdinaryString(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Failed to unserialize data');
        $serialized = 'foo';
        $this->serializationStrategy->deserialize($serialized);
    }
}
