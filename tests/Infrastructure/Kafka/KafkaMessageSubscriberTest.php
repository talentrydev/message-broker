<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Kafka;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaFactory;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaMessage;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaMessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;

class KafkaMessageSubscriberTest extends TestCase
{
    private KafkaFactory $kafkaFactory;
    private KafkaMessageSubscriber $kafkaMessageSubscriber;
    private KafkaMessagePublisher $kafkaMessagePublisher;
    private ChannelRegistry $channelRegistry;
    private string $payload = 'foo';
    private Channel $channel;
    private ?Message $obtainedMessage;

    protected function setUp(): void
    {
        $this->kafkaFactory = new KafkaFactory(getenv('KAFKA_BROKER_LIST'));
        $this->channelRegistry = new ChannelRegistry();
        $this->kafkaMessageSubscriber = new KafkaMessageSubscriber(
            $this->kafkaFactory,
            $this->channelRegistry,
        );
        $this->kafkaMessagePublisher = new KafkaMessagePublisher(
            $this->kafkaFactory,
            $this->channelRegistry,
        );
        $this->channel = new Channel(uniqid());
    }

    public function testGetMessageFromChannelWithoutDeletion(): void
    {
        $this->givenMessageIsPublished($this->payload);
        $this->whenMessageIsObtained(withDeletion: false);
        $this->expectObtainedMessagePayload($this->payload);

        //another subscriber should still have access to this message
        $this->whenMessageIsObtainedByAnotherSubscriber();
        $this->expectObtainedMessagePayload($this->payload);
    }

    public function testGetMessageFromChannelWithDeletion(): void
    {
        $this->givenMessageIsPublished($this->payload);
        $this->whenMessageIsObtained();
        $this->expectObtainedMessagePayload($this->payload);

        //another subscriber should not have access to this message
        $this->whenMessageIsObtainedByAnotherSubscriber();
        $this->expectNoMessageIsObtained();
    }

    public function testGetAllMessagesFromChannel(): void
    {
        $this->givenMessageIsPublished('first');
        $this->givenMessageIsPublished('second');
        $this->whenMessageIsObtained();
        $this->expectObtainedMessagePayload('first');
        $this->whenMessageIsObtained();
        $this->expectObtainedMessagePayload('second');
        $this->whenMessageIsObtained();
        $this->expectNoMessageIsObtained();

        //even though we reached the end of queue, calling getAllMessages will return all messages
        $messages = $this->kafkaMessageSubscriber->getAllMessagesFromChannel($this->channel);
        self::assertCount(2, $messages);
        self::assertSame('first', $messages[0]->getPayload());
        self::assertSame('second', $messages[1]->getPayload());
    }

    public function testDeleteMessage(): void
    {
        $this->givenMessageIsPublished($this->payload);
        $this->whenMessageIsObtained(withDeletion: false);
        $this->whenObtainedMessageIsDeleted();

        //another subscriber should not have access to this message
        $this->whenMessageIsObtainedByAnotherSubscriber();
        $this->expectNoMessageIsObtained();
    }

    public function testDeleteMessageOfInvalidType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(KafkaMessage::class . ' expected, got ' . Message::class);
        $message = new Message($this->channel, $this->payload, uniqid());
        $this->kafkaMessageSubscriber->deleteMessage($message, $message->getChannel());
    }

    public function testDeleteMessageWithMissingRdKafkaMessage(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('RdKafka\Message missing');
        $message = new KafkaMessage($this->channel, $this->payload, uniqid());
        $this->kafkaMessageSubscriber->deleteMessage($message, $message->getChannel());
    }

    private function givenMessageIsPublished(string $payload): void
    {
        $message = new Message($this->channel, $payload, uniqid());
        $this->kafkaMessagePublisher->sendMessage($message);

        //wait until the new topic is available to consumers
        //(by using a different group ID, we don't interfere with the consumer under test)
        $consumer = $this->kafkaFactory->generateConsumer(uniqid());
        $consumer->subscribe([$this->channel->getName()]);
        do {
            $message = $consumer->consume(timeout_ms: 100);
        } while ($message->err !== RD_KAFKA_RESP_ERR_NO_ERROR);
    }

    private function whenMessageIsObtained(bool $withDeletion = true): void
    {
        $this->obtainedMessage = $this->kafkaMessageSubscriber->getMessageFromChannel(
            channel: $this->channel,
            delete: $withDeletion,
        );
    }

    private function whenMessageIsObtainedByAnotherSubscriber(): void
    {
        unset($this->kafkaMessageSubscriber);
        $newSubscriber = new KafkaMessageSubscriber($this->kafkaFactory, $this->channelRegistry);
        $this->obtainedMessage = $newSubscriber->getMessageFromChannel(channel: $this->channel);
    }

    private function whenObtainedMessageIsDeleted(): void
    {
        $this->kafkaMessageSubscriber->deleteMessage($this->obtainedMessage, $this->channel);
    }

    public function expectObtainedMessagePayload(string $payload): void
    {
        self::assertSame($payload, $this->obtainedMessage->getPayload());
    }

    public function expectNoMessageIsObtained(): void
    {
        self::assertNull($this->obtainedMessage);
    }
}
