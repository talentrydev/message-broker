<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Kafka;

use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use ColinODell\PsrTestLogger\TestLogger;
use RdKafka\KafkaConsumer;
use RdKafka\Producer;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\InMemoryFailedMessageSpool;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaFactory;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Serialization\Base64PhpSerializationStrategy;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Tests\Infrastructure\FailedMessageSpool\FailedMessageSpoolTester;
use Talentry\MessageBroker\Tests\Infrastructure\Logging\LoggerTraitTester;
use Talentry\MessageBroker\Tests\Mocks\MessageGenerator;

class KafkaMessagePublisherTest extends TestCase
{
    use LoggerTraitTester;
    use FailedMessageSpoolTester;

    private KafkaMessagePublisher $kafkaMessagePublisher;
    private KafkaConsumer $consumer;
    private KafkaFactory $kafkaFactory;
    private FailedMessageSpool $failedMessageSpool;
    private Message $message;
    private TestLogger $logger;
    private ChannelRegistry $channelRegistry;
    private string $mockErrorMessage = 'error';

    protected function setUp(): void
    {
        $this->logger = new TestLogger();
        $this->failedMessageSpool = new InMemoryFailedMessageSpool();
        $this->channelRegistry = new ChannelRegistry();
        $messageGenerator = new MessageGenerator($this->channelRegistry);
        $this->message = $messageGenerator->generateMessage(channel: new Channel(uniqid()));
    }

    public function testSendMessage(): void
    {
        $this->givenRealKafkaEnvironment();
        $this->whenMessageIsSent();
        $this->expectMessageCanBeReceived();
    }

    public function testSendMessageWithException(): void
    {
        $this->givenKafkaProducerWillThrowException();
        $this->whenMessageIsSent();
        $this->assertSpooledMessageWasLogged($this->message, $this->mockErrorMessage);
        $this->assertMessageWasSpooled($this->message);
    }

    private function givenRealKafkaEnvironment(): void
    {
        $this->kafkaFactory = new KafkaFactory(getenv('KAFKA_BROKER_LIST'));
    }

    private function givenKafkaProducerWillThrowException(): void
    {
        $producer = $this->createMock(Producer::class);
        $producer->method('newTopic')->willThrowException(new Exception($this->mockErrorMessage));
        $this->kafkaFactory = $this->createMock(KafkaFactory::class);
        $this->kafkaFactory->method('generateProducer')->willReturn($producer);
    }

    private function whenMessageIsSent(): void
    {
        $this->getKafkaMessagePublisher()->sendMessage($this->message);
    }

    private function expectMessageCanBeReceived(): void
    {
        $this->getKafkaConsumer()->subscribe([$this->message->getChannel()->getName()]);

        do {
            $receivedMessage = $this->getKafkaConsumer()->consume(timeout_ms: 100);
        } while ($receivedMessage->err !== RD_KAFKA_RESP_ERR_NO_ERROR);

        $serializedMessage = (new Base64PhpSerializationStrategy())->serialize($this->message);
        self::assertSame($serializedMessage, $receivedMessage->payload);
        $this->assertMessageWasLogged($this->message);
    }

    private function getKafkaMessagePublisher(): KafkaMessagePublisher
    {
        if (!isset($this->kafkaMessagePublisher)) {
            $this->kafkaMessagePublisher = new KafkaMessagePublisher(
                kafkaFactory: $this->kafkaFactory,
                channelRegistry: $this->channelRegistry,
                failedMessageSpool: $this->failedMessageSpool,
                logger: $this->logger,
            );
        }

        return $this->kafkaMessagePublisher;
    }

    private function getKafkaConsumer(): KafkaConsumer
    {
        if (!isset($this->consumer)) {
            $this->consumer = $this->kafkaFactory->generateConsumer('testGroup');
        }

        return $this->consumer;
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    protected function getFailedMessageSpool(): FailedMessageSpool
    {
        return $this->failedMessageSpool;
    }
}
