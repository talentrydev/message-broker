<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\FailedMessageSpool;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\RedisFailedMessageSpool;
use Talentry\MessageBroker\Tests\Mocks\MockRedis;

class RedisFailedMessageSpoolTest extends TestCase
{
    public function testQueue(): void
    {
        $queue = new RedisFailedMessageSpool(new MockRedis());
        $message = new Message(new Channel('foo'), 'foo', 'k5TnY4Tq');
        $queue->push($message);

        $queuedMessage = $queue->pop();
        self::assertNotNull($queuedMessage);
        self::assertSame($message->getPayload(), $queuedMessage->getPayload());
        self::assertSame($message->getId(), $queuedMessage->getId());
        self::assertSame($message->getChannel()->getName(), $queuedMessage->getChannel()->getName());

        self::assertNull($queue->pop());
    }
}
