<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\FailedMessageSpool;

use PHPUnit\Framework\Assert;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\Domain\Message\Message;

trait FailedMessageSpoolTester
{
    public function assertMessageWasSpooled(Message $message): void
    {
        Assert::assertTrue($this->isMessageSpooled($message));
    }

    public function assertMessageWasNotSpooled(Message $message): void
    {
        Assert::assertFalse($this->isMessageSpooled($message));
    }

    public function isMessageSpooled(Message $message): bool
    {
        $spooled = false;
        $failedMessageSpool = $this->getFailedMessageSpool()->clone();
        while (($failedMessage = $failedMessageSpool->pop()) !== null) {
            if ($failedMessage->getPayloadId() === $message->getPayloadId()) {
                $spooled = true;
                break;
            }
        }

        return $spooled;
    }

    abstract protected function getFailedMessageSpool(): FailedMessageSpool;
}
