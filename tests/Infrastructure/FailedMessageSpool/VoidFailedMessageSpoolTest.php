<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\FailedMessageSpool;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\VoidFailedMessageSpool;

class VoidFailedMessageSpoolTest extends TestCase
{
    public function testQueue(): void
    {
        $queue = new VoidFailedMessageSpool();
        $queue->push(new Message(new Channel('foo'), 'foo', '1'));

        self::assertNull($queue->pop());
    }
}
