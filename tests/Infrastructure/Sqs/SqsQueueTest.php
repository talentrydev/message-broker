<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sqs;

use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Predis\Connection\ConnectionException;
use Predis\Connection\NodeConnectionInterface;
use Psr\Cache\CacheItemPoolInterface;
use ReflectionClass;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Talentry\MessageBroker\Infrastructure\Sqs\RedrivePolicy;
use Talentry\MessageBroker\Domain\Channel\RetentionPeriod;
use Talentry\MessageBroker\Domain\Exception\CreateChannelException;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueue;
use Talentry\MessageBroker\Tests\Mocks\MockSqsClient;

class SqsQueueTest extends TestCase
{
    private MockSqsClient|MockObject $sqsClient;
    private CacheItemPoolInterface $cachePool;
    private ?SqsQueue $sqsQueue = null;
    private ?SqsQueue $deadLetterQueue = null;
    private Exception $caughtException;
    private ?RetentionPeriod $retentionPeriod = null;
    private ?RedrivePolicy $redrivePolicy = null;
    private string $queueName = 'foo';
    private ?string $namespace = null;
    private int $minimumRetentionPeriod = 60;
    private int $maximumRetentionPeriod = 14 * 86400;

    protected function setUp(): void
    {
        $this->cachePool = new ArrayAdapter();
    }

    protected function tearDown(): void
    {
        if (isset($this->caughtException)) {
            throw $this->caughtException;
        }
    }

    public function testCreateQueueWithHealthyAwsApi(): void
    {
        $this->givenThatAwsApiIsHealthy();
        $this->givenThatQueueDoesNotExist();
        $this->whenQueueIsCreated();
        $this->expectThatQueueExists();
    }

    public function testCreateQueueWithUnhealthyAwsApi(): void
    {
        $this->givenThatAwsApiIsUnhealthy();
        $this->whenQueueIsCreated();
        $this->expectCreateChannelException();
    }

    public function testCreateExistingQueue(): void
    {
        $this->givenThatAwsApiIsHealthy();
        $this->givenThatQueueExists();
        $this->givenThatQueueIsNotCached();
        $this->whenQueueIsCreated();
        $this->expectThatQueueCreationWasNotPerformed();
    }

    public function testCreateQueueWithCaching(): void
    {
        $this->givenThatAwsApiIsUnhealthy(); //because with caching in place, we don't need to query AWS
        $this->givenThatQueueIsCached();
        $this->whenQueueIsCreated();
        $this->expectThatQueueHasUrlAndArn();
    }

    public function testCreateQueueWithCachingAndUnhealthyCache(): void
    {
        $this->givenThatAwsApiIsHealthy(); //we do need AWS this time, as fallback
        $this->givenThatCachePoolIsUnhealthy();
        $this->whenQueueIsCreated();
        $this->expectThatQueueHasUrlAndArn();
    }

    public function testCreateQueueWithNamespace(): void
    {
        $this->givenThatNamespaceIsConfigured();
        $this->givenThatAwsApiIsHealthy();
        $this->whenQueueIsCreated();
        $this->expectThatQueueNameContainsNamespace();
    }

    public function testCreateQueueWithRetentionPeriod(): void
    {
        $this->givenThatAwsApiIsHealthy();
        $this->givenThatQueueDoesNotExist();
        $this->givenCustomRetentionPeriod();
        $this->whenQueueIsCreated();
        $this->expectRetentionPeriodToBeConfigured();
    }

    public function testCreateQueueWithRedrivePolicy(): void
    {
        $this->givenThatAwsApiIsHealthy();
        $this->givenThatQueueDoesNotExist();
        $this->givenThatDeadLetterQueueExists();
        $this->givenCustomRedrivePolicyUsingDeadLetterQueue();
        $this->whenQueueIsCreated();
        $this->expectCustomRedrivePolicyToBeConfigured();
    }

    private function givenThatAwsApiIsHealthy(): void
    {
        $this->sqsClient = new MockSqsClient();
    }

    private function givenThatAwsApiIsUnhealthy(): void
    {
        $this->sqsClient = $this->createMock(SqsClient::class);
        $e = $this->createMock(SqsException::class);
        $this->sqsClient->method('__call')->willThrowException($e);
    }

    private function givenThatQueueExists(): void
    {
        $this->sqsClient->disableTracing();
        $this->createQueue($this->queueName);
        $this->sqsClient->enableTracing();
    }

    private function givenThatQueueDoesNotExist(): void
    {
        $this->assertQueueDoesNotExist($this->queueName);
    }

    private function givenThatDeadLetterQueueExists(): void
    {
        $this->sqsClient->disableTracing();
        $this->deadLetterQueue = $this->createQueue('dlq', RetentionPeriod::maximum());
        $this->sqsClient->enableTracing();
    }

    private function givenThatQueueIsCached(): void
    {
        $item = $this->cachePool->getItem($this->queueName)->set(['url' => 'foo', 'arn' => 'bar']);
        $this->cachePool->save($item);
    }

    private function givenThatQueueIsNotCached(): void
    {
        $this->cachePool->deleteItem($this->queueName);
    }

    private function givenThatNamespaceIsConfigured(): void
    {
        $this->namespace = 'foo';
    }

    private function givenThatCachePoolIsUnhealthy(): void
    {
        $this->cachePool = $this->createMock(CacheItemPoolInterface::class);
        $exception = new ConnectionException($this->createMock(NodeConnectionInterface::class));
        $this->cachePool->method('getItem')->willThrowException($exception);
    }

    private function givenCustomRetentionPeriod(): void
    {
        $this->retentionPeriod = RetentionPeriod::days(2);
    }

    private function givenCustomRedrivePolicyUsingDeadLetterQueue(): void
    {
        $this->redrivePolicy = new RedrivePolicy($this->deadLetterQueue, 3);
    }

    private function whenQueueIsCreated(): void
    {
        try {
            $this->sqsQueue = $this->createQueue($this->queueName, $this->retentionPeriod, $this->redrivePolicy);
        } catch (Exception $e) {
            $this->caughtException = $e;
        }
    }

    private function expectThatQueueExists(): void
    {
        $this->assertQueueExists($this->queueName);
    }

    private function expectThatQueueNameContainsNamespace(): void
    {
        $rc = new ReflectionClass($this->sqsQueue);
        $prop = $rc->getProperty('name');
        $prop->setAccessible(true);
        $actualValue = $prop->getValue($this->sqsQueue);
        $expectedValue = $this->namespace . '_' . $this->queueName;
        self::assertSame($expectedValue, $actualValue);
    }

    private function expectRetentionPeriodToBeConfigured(): void
    {
        $result = $this->sqsClient->getQueueUrl(['QueueName' => $this->queueName]);
        $result = $this->sqsClient->getQueueAttributes(['QueueUrl' => $result['QueueUrl']]);
        $expectedRetentionPeriod = $this->retentionPeriod->asInteger(
            $this->minimumRetentionPeriod,
            $this->maximumRetentionPeriod
        );
        self::assertSame($expectedRetentionPeriod, (int) $result['Attributes']['MessageRetentionPeriod']);
    }

    private function expectCustomRedrivePolicyToBeConfigured(): void
    {
        $result = $this->sqsClient->getQueueUrl(['QueueName' => $this->queueName]);
        $result = $this->sqsClient->getQueueAttributes(['QueueUrl' => $result['QueueUrl']]);
        $redrivePolicy = json_decode($result['Attributes']['RedrivePolicy'], true);
        self::assertSame($this->redrivePolicy->deadLetterQueue()->id(), $redrivePolicy['deadLetterTargetArn']);
        self::assertSame($this->redrivePolicy->maxReceiveCount(), $redrivePolicy['maxReceiveCount']);
    }

    private function expectCreateChannelException(): void
    {
        self::assertInstanceOf(CreateChannelException::class, $this->caughtException);
        unset($this->caughtException);
    }

    private function expectThatQueueHasUrlAndArn(): void
    {
        self::assertNotEmpty($this->sqsQueue->url());
        self::assertNotEmpty($this->sqsQueue->arn());
    }

    private function expectThatQueueCreationWasNotPerformed(): void
    {
        self::assertNotContains('createQueue', $this->sqsClient->getOperations());
    }

    private function assertQueueExists(string $queueName): void
    {
        $e = null;

        try {
            $this->sqsClient->getQueueUrl(['QueueName' => $queueName]);
        } catch (SqsException $e) {
        }

        self::assertNull($e);
    }

    private function assertQueueDoesNotExist(string $queueName): void
    {
        $exception = null;
        try {
            $this->sqsClient->getQueueUrl(['QueueName' => $queueName]);
        } catch (SqsException $exception) {
        }

        self::assertNotNull($exception);
        self::assertSame('AWS.SimpleQueueService.NonExistentQueue', $exception->getAwsErrorCode());
    }

    private function createQueue(
        string $name,
        ?RetentionPeriod $retentionPeriod = null,
        ?RedrivePolicy $redrivePolicy = null
    ): SqsQueue {
        return new SqsQueue(
            $this->sqsClient,
            $this->cachePool,
            $name,
            $this->namespace,
            $retentionPeriod,
            $redrivePolicy
        );
    }
}
