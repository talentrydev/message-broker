<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sqs;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Infrastructure\Serialization\Base64PhpSerializationStrategy;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessage;
use Talentry\MessageBroker\Tests\DomainAssert;

class SqsMessageTest extends TestCase
{
    public function testSerialization(): void
    {
        $serializer = new Base64PhpSerializationStrategy();
        $message = new SqsMessage(new Channel('test'), 'foo', uniqid());
        $serialized = $serializer->serialize($message);
        $unserialized = $serializer->deserialize($serialized);

        DomainAssert::assertMessagesAreEqual($message, $unserialized);
    }
}
