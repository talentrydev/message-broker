<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sqs;

use Aws\Result;
use Aws\Sqs\SqsClient;
use Exception;
use InvalidArgumentException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessage;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageMapper;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageSubscriber;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueue;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueueFactory;

class SqsMessageSubscriberUnitTest extends TestCase
{
    private const string CHANNEL_NAME = 'bc14f3c1-channel';
    private const string MESSAGE_PAYLOAD = '{"a6f73cca":"9d288f67"}';
    private const string MESSAGE_PAYLOAD_ID = '74a61de7-id';
    private const string RECEIPT_HANDLE = 'ad17daf4-receipt';

    private SqsClient|MockObject $sqsClient;
    private SqsQueueFactory|MockObject $sqsQueueFactory;
    private SqsMessageSubscriber $sut;
    private string $queueUrl = 'https://be3b6348.com/';
    private int $maximumVisibilityTimeout = 12 * 3600;

    protected function setUp(): void
    {
        $this->sqsClient = $this->createMock(SqsClient::class);
        $this->sqsQueueFactory = $this->createMock(SqsQueueFactory::class);
        $this->sut = new SqsMessageSubscriber(
            $this->sqsClient,
            $this->sqsQueueFactory,
            $this->createMock(LoggerInterface::class),
            new SqsMessageMapper(),
            new ChannelRegistry(),
        );
    }

    public function testGetMessage(): void
    {
        $sqsQueue = $this->createMock(SqsQueue::class);
        $message = new SqsMessage(new Channel(self::CHANNEL_NAME), self::MESSAGE_PAYLOAD, self::MESSAGE_PAYLOAD_ID);
        $message->setReceiptHandle(self::RECEIPT_HANDLE);

        $this->sqsQueueFactory->expects(self::exactly(2))
            ->method('fromChannel')
            ->with(new Channel(self::CHANNEL_NAME))
            ->willReturn($sqsQueue);

        $sqsQueue->expects(self::exactly(2))
            ->method('url')
            ->willReturn($this->queueUrl);

        $attributes = [
            'QueueUrl' => $this->queueUrl,
            'MaxNumberOfMessages' => 1,
            'WaitTimeSeconds' => 0,
            'VisibilityTimeout' => 1800,
        ];
        $clientResult = $this->createMock(Result::class);
        $this->sqsClient
            ->expects(self::exactly(2))
            ->method('__call')
            ->willReturnCallback(function ($name, array $args) use ($attributes, $clientResult) {
                if ($name === 'receiveMessage') {
                    self::assertSame($attributes, $args[0]);

                    return $clientResult;
                } elseif ($name = 'deleteMessage') {
                    self::assertSame(
                        ['QueueUrl' => $this->queueUrl, 'ReceiptHandle' => self::RECEIPT_HANDLE],
                        $args[0],
                    );

                    return null;
                } else {
                    throw new Exception('Unexpected method call: ' . $name);
                }
            });

        $clientResult->expects(self::once())
            ->method('get')
            ->with('Messages')
            ->willReturn([['Body' => base64_encode(serialize($message)), 'ReceiptHandle' => self::RECEIPT_HANDLE]]);

        $result = $this->sut->getMessageFromChannel(new Channel(self::CHANNEL_NAME));

        self::assertEquals($message, $result);
    }

    #[DataProvider('dataProviderForTestDeleteMessage')]
    public function testDeleteMessage(Message $message): void
    {
        if (!$message instanceof SqsMessage) {
            $this->expectException(InvalidArgumentException::class);
            $this->expectExceptionMessage(SqsMessage::class . ' expected, got ' . $message::class);
        } elseif ($message->getReceiptHandle() === null) {
            $this->expectException(InvalidArgumentException::class);
        } else {
            $sqsQueue = $this->createMock(SqsQueue::class);

            $this->sqsQueueFactory->expects(self::once())
                ->method('fromChannel')
                ->with(new Channel(self::CHANNEL_NAME))
                ->willReturn($sqsQueue);

            $sqsQueue->expects(self::once())
                ->method('url')
                ->willReturn($this->queueUrl);

            $this->sqsClient->expects(self::once())
                ->method('__call')
                ->with(
                    'deleteMessage',
                    [['QueueUrl' => $this->queueUrl, 'ReceiptHandle' => self::RECEIPT_HANDLE]],
                )
            ;
        }

        $this->sut->deleteMessage($message, $message->getChannel());
    }

    public static function dataProviderForTestDeleteMessage(): array
    {
        $genericMessage = new Message(new Channel(self::CHANNEL_NAME), self::MESSAGE_PAYLOAD, self::MESSAGE_PAYLOAD_ID);
        $sqsMessage = new SqsMessage(new Channel(self::CHANNEL_NAME), self::MESSAGE_PAYLOAD, self::MESSAGE_PAYLOAD_ID);
        $sqsMessageWithReceiptHandle = clone $sqsMessage;
        $sqsMessageWithReceiptHandle->setReceiptHandle(self::RECEIPT_HANDLE);

        return [
            [$genericMessage],
            [$sqsMessage],
            [$sqsMessageWithReceiptHandle],
        ];
    }

    public function testGetAllMessagesFromChannel(): void
    {
        $sqsQueue = $this->createMock(SqsQueue::class);

        $this->sqsQueueFactory->expects(self::exactly(6))
            ->method('fromChannel')
            ->with(new Channel(self::CHANNEL_NAME))
            ->willReturn($sqsQueue);

        $sqsQueue->expects(self::exactly(6))
            ->method('url')
            ->willReturn($this->queueUrl);

        //here we simulate a queue with 3 messages, but it returns the first message over and over again;
        //on 1st, 3rd and 5th call it will return messages 1, 2 and 3;
        //on 2nd and 4th call, it will return the message 1 again;
        //on final, 6th, call, it will return null, signifying that there are no more messages in queue;
        //as a result, we expect the visibility timeout to be increased twice: first to 10, then to 15 seconds
        $this->sqsClient->expects(self::exactly(6))
            ->method('__call')
            ->willReturnCallback(function ($method, $params) {
                static $counter = 0;
                static $expectedVisibilityTimeout = 5;
                static $nextMessageId = 1;
                self::assertSame('receiveMessage', $method);

                if (++$counter > 5) {
                    return $this->generateSqsClientResult();
                }

                $expectedAttributes = [
                    'QueueUrl' => $this->queueUrl,
                    'MaxNumberOfMessages' => 1,
                    'WaitTimeSeconds' => 0,
                    'VisibilityTimeout' => $expectedVisibilityTimeout,
                ];
                $attributes = $params[0];
                self::assertSame($expectedAttributes, $attributes);

                $repeatedMessageId = '1';
                if ($counter % 2 === 0) {
                    $expectedVisibilityTimeout += 5;
                    $messageId = $repeatedMessageId;
                } else {
                    $messageId = (string) $nextMessageId++;
                }

                $message = new SqsMessage(new Channel(self::CHANNEL_NAME), self::MESSAGE_PAYLOAD, $messageId);

                return $this->generateSqsClientResult($message);
            });

        $messages = $this->sut->getAllMessagesFromChannel(new Channel(self::CHANNEL_NAME));

        //received messages should not contain duplicates:
        //message 1 appears only once, even though it was delivered 3 times
        self::assertCount(3, $messages);
        self::assertSame('1', $messages[0]->getPayloadId());
        self::assertSame('2', $messages[1]->getPayloadId());
        self::assertSame('3', $messages[2]->getPayloadId());
    }

    public function testMaximumVisibilityTimeoutWhenGettingAllMessagesFromChannel(): void
    {
        $sqsQueue = $this->createMock(SqsQueue::class);

        $this->sqsQueueFactory
            ->method('fromChannel')
            ->with(new Channel(self::CHANNEL_NAME))
            ->willReturn($sqsQueue);

        $sqsQueue
            ->method('url')
            ->willReturn($this->queueUrl);

        $this->sqsClient
            ->method('__call')
            ->willReturnCallback(function ($method, $params) {
                static $counter = 0;
                static $expectedVisibilityTimeout = 5;
                self::assertSame('receiveMessage', $method);

                $expectedAttributes = [
                    'QueueUrl' => $this->queueUrl,
                    'MaxNumberOfMessages' => 1,
                    'WaitTimeSeconds' => 0,
                    'VisibilityTimeout' => $expectedVisibilityTimeout,
                ];

                $attributes = $params[0];
                self::assertSame($expectedAttributes, $attributes);
                if (++$counter > 1) {
                    $expectedVisibilityTimeout += 5;
                    if ($expectedVisibilityTimeout > $this->maximumVisibilityTimeout) {
                        $expectedVisibilityTimeout = $this->maximumVisibilityTimeout;
                    }
                }

                //after 8641 repetitions, the visibility timeout should reach the maximum value
                if ($counter > $this->maximumVisibilityTimeout / 5 + 1) {
                    self::assertSame($this->maximumVisibilityTimeout, $attributes['VisibilityTimeout']);

                    return $this->generateSqsClientResult();
                }

                $message = new SqsMessage(
                    new Channel(self::CHANNEL_NAME),
                    self::MESSAGE_PAYLOAD,
                    self::MESSAGE_PAYLOAD_ID,
                );

                return $this->generateSqsClientResult($message);
            });

        $messages = $this->sut->getAllMessagesFromChannel(new Channel(self::CHANNEL_NAME));
        //since the queue delivered the same message over and over again, we should only have one message
        self::assertCount(1, $messages);
    }

    private function generateSqsClientResult(?SqsMessage $message = null): Result
    {
        $clientResult = $this->createMock(Result::class);
        $payload = [];
        if ($message !== null) {
            $payload[] = [
                'Body' => base64_encode(serialize($message)),
                'ReceiptHandle' => uniqid(),
            ];
        }
        $clientResult
            ->method('get')
            ->with('Messages')
            ->willReturn($payload);

        return $clientResult;
    }
}
