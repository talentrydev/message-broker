<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sqs;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageMapper;
use Talentry\MessageBroker\Tests\DomainAssert;

class SqsMessageMapperTest extends TestCase
{
    public function testToSqsMessage(): void
    {
        $message = new Message(new Channel('test'), 'foo', uniqid());
        $mapper = new SqsMessageMapper();
        $sqsMessage = $mapper->toSqsMessage($message);

        DomainAssert::assertMessagesAreEqual($message, $sqsMessage);
    }
}
