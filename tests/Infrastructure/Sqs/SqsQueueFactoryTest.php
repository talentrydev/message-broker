<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sqs;

use Aws\Sqs\Exception\SqsException;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;
use ReflectionClass;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueue;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueueFactory;
use Talentry\MessageBroker\Tests\Mocks\MockSqsClient;

class SqsQueueFactoryTest extends TestCase
{
    private SqsQueueFactory $factory;
    private MockSqsClient $sqsClient;
    private Channel $channel;
    private ?SqsQueue $sqsQueue = null;
    private CacheItemPoolInterface $cache;
    private string $dlqSuffix = '_dead_letter_queue';

    protected function setUp(): void
    {
        $this->sqsClient = new MockSqsClient();
        $this->cache = new ArrayAdapter();
        $channelRegistry = new ChannelRegistry($this->dlqSuffix);
        $this->factory = new SqsQueueFactory($this->sqsClient, $channelRegistry, $this->cache);
    }

    public function testFromRegularChannel(): void
    {
        $this->givenRegularChannel();
        $this->whenQueueIsGeneratedFromChannel();
        $this->expectThatQueueExistsForChannel();
        $this->expectQueueToBeCached();
        $this->expectRedrivePolicyToBeConfigured();
    }

    public function testFromDlqChannel(): void
    {
        $this->givenDlqChannel();
        $this->whenQueueIsGeneratedFromChannel();
        $this->expectThatQueueExistsForChannel();
        $this->expectQueueToBeCached();
        $this->expectRedrivePolicyNotToBeConfigured();
    }

    public function testCreateDeadLetterQueue(): void
    {
        $this->givenDlqChannel();
        $this->whenDeadLetterQueueIsGenerated();
        $this->expectThatDeadLetterQueueExists();
        $this->expectQueueToBeCached();
        $this->expectRetentionPeriodToBeSetToMaximumValue();
    }

    private function givenRegularChannel(): void
    {
        $this->channel = new Channel('foo');
    }

    private function givenDlqChannel(): void
    {
        $this->channel = new Channel('foo' . $this->dlqSuffix);
    }

    private function whenQueueIsGeneratedFromChannel(): void
    {
        $this->sqsQueue = $this->factory->fromChannel($this->channel);
    }

    private function whenDeadLetterQueueIsGenerated(): void
    {
        $this->sqsQueue = $this->factory->createDeadLetterQueue($this->channel);
    }

    private function expectThatQueueExistsForChannel(): void
    {
        $this->assertQueueExists($this->channel->getName());
    }

    private function expectThatDeadLetterQueueExists(): void
    {
        $this->assertQueueExists($this->channel->getName() . $this->dlqSuffix);
    }

    private function expectQueueToBeCached(): void
    {
        $rc = new ReflectionClass($this->sqsQueue);
        $prop = $rc->getProperty('name');
        $prop->setAccessible(true);
        $queueName = $prop->getValue($this->sqsQueue);

        $item = $this->cache->getItem($queueName);
        self::assertTrue($item->isHit());
    }

    private function expectRetentionPeriodToBeSetToMaximumValue(): void
    {
        $result = $this->sqsClient->getQueueAttributes(['QueueUrl' => $this->sqsQueue->url()]);
        self::assertSame(14 * 86400, (int) $result['Attributes']['MessageRetentionPeriod']);
    }

    private function expectRedrivePolicyToBeConfigured(): void
    {
        $dlq = $this->factory->createDeadLetterQueue($this->channel);
        $result = $this->sqsClient->getQueueAttributes(['QueueUrl' => $this->sqsQueue->url()]);
        $redrivePolicy = json_decode($result['Attributes']['RedrivePolicy'], true);
        self::assertSame($dlq->id(), $redrivePolicy['deadLetterTargetArn']);
        self::assertSame(1, $redrivePolicy['maxReceiveCount']);
    }

    private function expectRedrivePolicyNotToBeConfigured(): void
    {
        $result = $this->sqsClient->getQueueAttributes(['QueueUrl' => $this->sqsQueue->url()]);
        self::assertArrayNotHasKey('RedrivePolicy', $result['Attributes']);
    }

    private function assertQueueExists(string $queueName): void
    {
        $e = null;

        try {
            $this->sqsClient->getQueueUrl(['QueueName' => $queueName]);
        } catch (SqsException $e) {
        }
        self::assertNull($e);
    }
}
