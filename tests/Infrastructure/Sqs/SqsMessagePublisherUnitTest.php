<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sqs;

use Aws\Sqs\SqsClient;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageAttributesProvider;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageMapper;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueue;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueueFactory;

class SqsMessagePublisherUnitTest extends TestCase
{
    private SqsClient|MockObject $sqsClient;
    private SqsMessageMapper $messageMapper;
    private SqsQueueFactory|MockObject $sqsQueueFactory;
    private LoggerInterface|MockObject $logger;
    private SqsMessagePublisher $sut;
    private FailedMessageSpool|MockObject $failedMessageSpool;
    private MessageParser|MockObject $messageParser;
    private string $channelName = 'bc14f3c1-channel';
    private string $messagePayload = '{"a6f73cca":"9d288f67"}';
    private string $messagePayloadId = '74a61de7-id';
    private string $queueUrl = 'https://be3b6348.com/';

    protected function setUp(): void
    {
        $this->sqsClient = $this->createMock(SqsClient::class);
        $this->messageMapper = new SqsMessageMapper();
        $this->sqsQueueFactory = $this->createMock(SqsQueueFactory::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->messageParser = $this->createMock(MessageParser::class);
        $this->failedMessageSpool = $this->createMock(FailedMessageSpool::class);
        $channelRegistry = new ChannelRegistry();
        $this->sut = new SqsMessagePublisher(
            $this->sqsClient,
            $this->messageMapper,
            new SqsMessageAttributesProvider($this->messageParser),
            $this->sqsQueueFactory,
            $this->failedMessageSpool,
            $this->logger,
            $channelRegistry,
        );
    }

    public function testGetChannel(): void
    {
        $result = new Channel($this->channelName);

        self::assertEquals(new Channel($this->channelName), $result);
    }

    public function testSendMessage(): void
    {
        $sqsQueue = $this->createMock(SqsQueue::class);
        $channel = new Channel($this->channelName);
        $message = new Message($channel, $this->messagePayload, $this->messagePayloadId);

        $this->sqsQueueFactory->expects(self::once())
            ->method('fromChannel')
            ->with($channel)
            ->willReturn($sqsQueue);

        $this->messageParser
            ->expects(self::once())
            ->method('getTenantId')
            ->willReturn('1');

        $this->messageParser
            ->expects(self::once())
            ->method('getMessageType')
            ->willReturn('SomeCommand');

        $sqsQueue->expects(self::once())
            ->method('url')
            ->willReturn($this->queueUrl);

        $this->sqsClient->expects(self::once())
            ->method('__call')
            ->willReturnCallback(function (string $method, array $arguments) use ($message) {
                $arguments = $arguments[0];
                self::assertSame($this->queueUrl, $arguments['QueueUrl']);
                /** @var Message $decodedMessage */
                $decodedMessage = unserialize(base64_decode($arguments['MessageBody']));
                self::assertSame($decodedMessage->getChannel()->getName(), $message->getChannel()->getName());
                self::assertSame($decodedMessage->getPayload(), $message->getPayload());
                self::assertSame($decodedMessage->getPayloadId(), $message->getPayloadId());

                self::assertEqualsCanonicalizing(
                    [
                        'MessageId' => [
                            'DataType' => 'String',
                            'StringValue' => 'bc14f3c1-channel_74a61de7-id'
                        ],
                        'TenantId' => [
                            'DataType' => 'String',
                            'StringValue' => '1'
                        ],
                        'MessageType' => [
                            'DataType' => 'String',
                            'StringValue' => 'SomeCommand'
                        ],
                    ],
                    $arguments['MessageAttributes']
                );
            })
        ;

        $this->sut->sendMessage($message);
    }

    public function testSendMessageException(): void
    {
        $sqsQueue = $this->createMock(SqsQueue::class);
        $this->sqsQueueFactory->method('fromChannel')->willReturn($sqsQueue);

        $sqsQueue->method('url')->willReturn($this->queueUrl);

        $this->sqsClient->method('__call')
            ->with('sendMessage')
            ->willThrowException(new Exception());

        $message = new Message(new Channel($this->channelName), $this->messagePayload, $this->messagePayloadId);
        $this->failedMessageSpool->expects(self::once())
            ->method('push')
            ->with($this->messageMapper->toSqsMessage($message));

        $this->logger->expects(self::once())
            ->method('critical');

        $this->sut->sendMessage($message);
    }
}
