<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sqs;

use Aws\Sqs\SqsClient;
use InvalidArgumentException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\VisibilityTimeout;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageMapper;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageSubscriber;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueueFactory;
use Talentry\MessageBroker\Tests\Mocks\MockSqsClient;

class SqsVisibilityTimeoutTest extends TestCase
{
    private SqsMessageSubscriber $sqsMessageSubscriber;
    private SqsClient $sqsClient;

    protected function setUp(): void
    {
        $this->sqsClient = new MockSqsClient();
        $sqsQueueFactory = new SqsQueueFactory($this->sqsClient, new ChannelRegistry());
        $this->sqsMessageSubscriber = new SqsMessageSubscriber(
            $this->sqsClient,
            $sqsQueueFactory,
            new NullLogger(),
            new SqsMessageMapper(),
            new ChannelRegistry(),
        );
    }

    #[DataProvider('dataProvider')]
    public function testVisibilityTimeout(
        VisibilityTimeout $visibilityTimeout,
        int $expectedVisibilityTimeout,
        bool $expectException = false
    ): void {
        if ($expectException) {
            $this->expectException(InvalidArgumentException::class);
        }

        $channel = new Channel('test');
        $this->sqsMessageSubscriber->getMessageFromChannel($channel, true, null, $visibilityTimeout);

        self::assertSame($expectedVisibilityTimeout, $this->sqsClient->getRequestedVisibilityTimeout());
    }

    public static function dataProvider(): array
    {
        return [
            [VisibilityTimeout::inSeconds(0), 0],
            [VisibilityTimeout::inSeconds(1), 1],
            [VisibilityTimeout::inSeconds(12 * 3600 + 1), 0, true],
            [VisibilityTimeout::maximumValue(), 12 * 3600],
            [VisibilityTimeout::minimumValue(), 0],
            [VisibilityTimeout::defaultValue(), 30 * 60],
        ];
    }
}
