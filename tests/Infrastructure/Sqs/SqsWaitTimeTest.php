<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Sqs;

use Aws\Sqs\SqsClient;
use InvalidArgumentException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\WaitTime;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageMapper;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageSubscriber;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueueFactory;
use Talentry\MessageBroker\Tests\Mocks\MockSqsClient;

class SqsWaitTimeTest extends TestCase
{
    private SqsMessageSubscriber $sqsMessageSubscriber;
    private SqsClient $sqsClient;

    protected function setUp(): void
    {
        $this->sqsClient = new MockSqsClient();
        $sqsQueueFactory = new SqsQueueFactory($this->sqsClient, new ChannelRegistry());
        $this->sqsMessageSubscriber = new SqsMessageSubscriber(
            $this->sqsClient,
            $sqsQueueFactory,
            new NullLogger(),
            new SqsMessageMapper(),
            new ChannelRegistry(),
        );
    }

    #[DataProvider('dataProvider')]
    public function testWaitTime(WaitTime $waitTime, int $expectedWaitTime, bool $expectException = false): void
    {
        if ($expectException) {
            $this->expectException(InvalidArgumentException::class);
        }

        $channel = new Channel('test');
        $this->sqsMessageSubscriber->getMessageFromChannel($channel, true, $waitTime);

        self::assertSame($expectedWaitTime, $this->sqsClient->getRequestedWaitTime());
    }

    public static function dataProvider(): array
    {
        return [
            [WaitTime::inSeconds(0), 0],
            [WaitTime::inSeconds(1), 1],
            [WaitTime::inSeconds(21), 0, true],
            [WaitTime::maximumValue(), 20],
            [WaitTime::minimumValue(), 0],
            [WaitTime::defaultValue(), 0],
        ];
    }
}
