<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Tests\Infrastructure\Logging;

use PHPUnit\Framework\Assert;
use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\Domain\Message\Message;

trait LoggerTraitTester
{
    private function assertMessageWasLogged(Message $message): void
    {
        Assert::assertTrue($this->getLogger()->hasDebug(sprintf(
            'Sent message to channel %s with payLoadId %s',
            $message->getChannelName(),
            $message->getPayloadId()
        )));
    }

    public function assertSpooledMessageWasLogged(Message $message, string $expectedError): void
    {
        Assert::assertTrue($this->getLogger()->hasCritical([
            'message' => 'Message spooled due to the following error: ' . $expectedError,
            'context' => [
                'channel' => $message->getChannelName(),
                'payloadId' => $message->getPayloadId(),
            ],
        ]));
    }

    abstract protected function getLogger(): LoggerInterface;
}
