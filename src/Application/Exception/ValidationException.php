<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Application\Exception;

use Exception;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends Exception
{
    /**
     * @param ConstraintViolationListInterface<ConstraintViolationInterface> $violations
     */
    public function __construct(
        private readonly ConstraintViolationListInterface $violations,
    ) {
        parent::__construct('Validation exception');
    }

    /**
     * @return ConstraintViolationListInterface<ConstraintViolationInterface>
     */
    public function violations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
