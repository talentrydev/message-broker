<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Application\Channel;

use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\Channel\ChannelFactory as ChannelFactoryInterface;
use Talentry\MessageBroker\Domain\Channel\Channel as ChannelImplementation;

class ChannelFactory implements ChannelFactoryInterface
{
    public function createChannel(string $name): Channel
    {
        return new ChannelImplementation($name);
    }
}
