<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\MessageParser;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;

class FallbackMessageParser implements MessageParser
{
    public function getTenantId(Message $message): ?string
    {
        return null;
    }

    public function getMessageType(Message $message): string
    {
        return 'n/a';
    }

    public function getKibanaLink(Message $message): string
    {
        return 'n/a';
    }

    public function supports(Message $message): bool
    {
        return true;
    }

    public function parse(Message $message): string
    {
        return $message->getPayload();
    }
}
