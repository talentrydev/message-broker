<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\MessageParser;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\Domain\Exception\UnsupportedMessageException;

class CompositeMessageParser implements MessageParser
{
    /**
     * @var MessageParser[]
     */
    private array $parsers = [];

    public function __construct(
        private readonly FallbackMessageParser $fallbackMessageParser,
    ) {
    }

    public function registerParser(MessageParser $parser): void
    {
        $this->parsers[] = $parser;
    }

    public function getTenantId(Message $message): ?string
    {
        return $this->getParserForMessage($message)->getTenantId($message);
    }

    public function getMessageType(Message $message): string
    {
        return $this->getParserForMessage($message)->getMessageType($message);
    }

    public function getKibanaLink(Message $message): string
    {
        return $this->getParserForMessage($message)->getKibanaLink($message);
    }

    public function parse(Message $message): object|string
    {
        return $this->getParserForMessage($message)->parse($message);
    }

    public function supports(Message $message): bool
    {
        foreach ($this->getParsers() as $parser) {
            if ($parser->supports($message)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return MessageParser[]
     */
    private function getParsers(): array
    {
        return array_merge($this->parsers, [$this->fallbackMessageParser]);
    }

    /**
     * @throws UnsupportedMessageException
     */
    private function getParserForMessage(Message $message): MessageParser
    {
        foreach ($this->getParsers() as $parser) {
            if ($parser->supports($message)) {
                return $parser;
            }
        }

        throw new UnsupportedMessageException('Unsupported message provided to ' . __CLASS__);
    }
}
