<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\FailedMessageSpool;

use Predis\ClientInterface;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\Infrastructure\Serialization\PhpSerializationStrategy;

class RedisFailedMessageSpool implements FailedMessageSpool
{
    private const string QUEUE_NAME = 'REDIS_FAILED_MESSAGE_SPOOL';

    private MessageSerializationStrategy $serializationStrategy;

    public function __construct(
        private readonly ClientInterface $redis,
    ) {
        $this->serializationStrategy = new PhpSerializationStrategy();
    }

    public function push(Message $message): void
    {
        $this->redis->rpush(
            self::QUEUE_NAME,
            [$this->serializationStrategy->serialize($message)]
        );
    }

    public function pop(): ?Message
    {
        $payload = $this->redis->lpop(self::QUEUE_NAME);

        if ($payload === null) {
            return null;
        }

        return $this->serializationStrategy->deserialize($payload);
    }
}
