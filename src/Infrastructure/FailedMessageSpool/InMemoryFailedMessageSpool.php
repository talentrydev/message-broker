<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\FailedMessageSpool;

use SplQueue;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;

class InMemoryFailedMessageSpool implements FailedMessageSpool
{
    private SplQueue $queue;

    public function __construct()
    {
        $this->queue = new SplQueue();
    }

    public function push(Message $message): void
    {
        $this->queue->enqueue($message);
    }

    public function pop(): ?Message
    {
        if ($this->queue->isEmpty()) {
            return null;
        }

        return $this->queue->dequeue();
    }

    public function clone(): self
    {
        $clone = new self();
        $clone->queue = clone $this->queue;

        return $clone;
    }
}
