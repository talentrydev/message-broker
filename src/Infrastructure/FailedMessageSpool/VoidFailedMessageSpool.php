<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\FailedMessageSpool;

use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;

class VoidFailedMessageSpool implements FailedMessageSpool
{
    public function push(Message $message): void
    {
    }

    public function pop(): ?Message
    {
        return null;
    }
}
