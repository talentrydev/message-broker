<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Logging;

use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;

trait LoggerTrait
{
    protected function logGetMessage(Channel $channel, bool $exists): void
    {
        $message = sprintf(
            'Getting message for channel %s. ',
            $channel->getName()
        );

        if ($exists) {
            $message .= 'Exists.';
        } else {
            $message .= 'Does not exist';
        }

        $this->getLogger()->debug($message);
    }
    protected function logSendMessage(Message $message): void
    {
        $this->getLogger()->debug(
            sprintf(
                'Sent message to channel %s with payLoadId %s',
                $message->getChannel()->getName(),
                $message->getPayloadId()
            )
        );
    }

    protected function logPushToFailedMessageSpool(Message $message, string $errorMessage): void
    {
        $this->getLogger()->critical(
            sprintf(
                'Message spooled due to the following error: %s',
                $errorMessage
            ),
            [
                'channel' => $message->getChannel()->getName(),
                'payloadId' => $message->getPayloadId(),
            ],
        );
    }

    abstract protected function getLogger(): LoggerInterface;
}
