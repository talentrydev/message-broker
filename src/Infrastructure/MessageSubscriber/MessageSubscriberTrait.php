<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\MessageSubscriber;

use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\WaitTime\WaitTime;
use Talentry\MessageBroker\Domain\Message\VisibilityTimeout;

trait MessageSubscriberTrait
{
    private int $initialVisibilityTimeoutInSeconds = 5;
    private int $visibilityTimeoutIncrementInSeconds = 5;

    /**
     * @return Message[]
     */
    public function getAllMessagesFromChannel(
        Channel $channel,
        bool $delete = false,
    ): array {
        $visibilityTimeout = $this->initialVisibilityTimeoutInSeconds;

        $messages = [];

        while (true) {
            $message = $this->getMessageFromChannel(
                channel: $channel,
                delete: $delete,
                visibilityTimeout: VisibilityTimeout::inSeconds($visibilityTimeout),
            );

            if ($message === null) {
                break;
            }

            if (array_key_exists($message->getId(), $messages)) {
                $visibilityTimeout += $this->visibilityTimeoutIncrementInSeconds;
                if ($visibilityTimeout > $this->getMaximumVisibilityTimeout()) {
                    $visibilityTimeout = $this->getMaximumVisibilityTimeout();
                }
                continue;
            }

            $messages[$message->getId()] = $message;
        }

        return array_values($messages);
    }

    abstract public function getMessageFromChannel(
        Channel $channel,
        bool $delete = true,
        ?WaitTime $waitTime = null,
        ?VisibilityTimeout $visibilityTimeout = null,
    ): ?Message;

    abstract public function getMaximumVisibilityTimeout(): int;
}
