<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sns;

use Aws\CommandPool;
use Aws\Exception\AwsException;
use Aws\ResultInterface;
use Aws\Sns\Exception\SnsException;
use Aws\Sns\SnsClient;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\Infrastructure\Logging\LoggerTrait;

class SnsMessagePublisher implements MessagePublisher
{
    use LoggerTrait;

    public function __construct(
        private readonly SnsClient $snsClient,
        private readonly FailedMessageSpool $failedMessageSpool,
        private readonly SnsPayloadGenerator $snsPayloadGenerator,
        private readonly LoggerInterface $logger,
        private readonly string $snsTopicArn,
    ) {
    }

    public function sendMessage(Message $message): void
    {
        try {
            $payload = $this->snsPayloadGenerator->generateSnsPayload($message, $this->snsTopicArn);
            $this->snsClient->publish($payload->toArray());
            $this->logSendMessage($message);
        } catch (SnsException $e) {
            $this->getFailedMessageSpool()->push($message);
            $this->logPushToFailedMessageSpool($message, $e->getMessage());
        }
    }

    public function batchSendMessages(array $messages): void
    {
        $payloadGenerator = $this->snsPayloadGenerator->generateSnsBatchPayloads($messages, $this->snsTopicArn);

        while (($payloads = $payloadGenerator->current()) !== null) {
            // The AWS SDK has memory leaks here and there so it's recommended to call gc_collection_cycles manually
            // @see https://github.com/aws/aws-sdk-php/issues/1273
            gc_collect_cycles();

            $commandGenerator = function (SnsBatchPayload ...$payloads): \Generator {
                foreach ($payloads as $payload) {
                    yield $this->snsClient->getCommand(
                        'PublishBatch',
                        [
                            ...$payload->toArray(),
                            'MessageIds' => $payload->getMessageIds()
                        ]
                    );
                }
            };

            // executes 40 requests at the same time at any given time
            $pool = new CommandPool(
                $this->snsClient,
                $commandGenerator(...$payloads),
                [
                    'concurrency' => 40,
                    'fulfilled' => function (ResultInterface $result) use ($messages) {
                        /** @var array<string,array<string,string>> $successfulMessages */
                        $successfulMessages = $result['Successful'];
                        foreach ($successfulMessages as $messageData) {
                            $message = $this->getMessageById($messages, $messageData['Id']);
                            $this->logSendMessage($message);
                        }
                        /** @var array<string,array<string,string>> $failedMessages */
                        $failedMessages = $result['Failed'];
                        foreach ($failedMessages as $messageData) {
                            $message = $this->getMessageById($messages, $messageData['Id']);
                            $this->getFailedMessageSpool()->push($message);
                            $this->logPushToFailedMessageSpool($message, $messageData['Code']);
                        }
                    },
                    'rejected' => function (AwsException $reason) use ($messages) {
                        $messageIds = $reason->getCommand()->toArray()['MessageIds'];
                        foreach ($messageIds as $messageId) {
                            $message = $this->getMessageById($messages, $messageId);
                            $this->getFailedMessageSpool()->push($message);
                            $this->logPushToFailedMessageSpool($message, $reason->getMessage());
                        }
                    },
                ]
            );

            $pool->promise()->wait();

            $payloadGenerator->next();
        }
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function name(): string
    {
        return 'sns';
    }

    public function getFailedMessageSpool(): FailedMessageSpool
    {
        return $this->failedMessageSpool;
    }

    /**
     * @param Message[] $messages
     */
    private function getMessageById(array $messages, string $id): Message
    {
        foreach ($messages as $message) {
            if ($message->getId() === $id) {
                return $message;
            }
        }

        throw new InvalidArgumentException(sprintf('Message with ID %s not found', $id));
    }
}
