<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sns;

class SnsBatchPayloadEntry
{
    /**
     * @param array<string,mixed> $messageAttributes
     */
    public function __construct(
        private readonly string $messageId,
        private readonly string $message,
        private readonly array $messageAttributes,
    ) {
    }

    /**
     * @return array<string,string|array<string,mixed>>
     */
    public function toArray(): array
    {
        return [
            'Id' => $this->messageId,
            'Message' => $this->message,
            'MessageAttributes' => $this->messageAttributes,
        ];
    }

    public function getMessageId(): string
    {
        return $this->messageId;
    }

    public function size(): int
    {
        return mb_strlen($this->message, '8bit');
    }
}
