<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sns;

use Talentry\MessageBroker\ApplicationInterface\Message\MessageTooLargeException;

class SnsPayload
{
    // in bytes; actual limit is 262144 bytes, but we add a margin of error
    // This was previously 200k, which still caused problems
    public const int PAYLOAD_MAX_SIZE = 180_000;

    /**
     * @param array<string,mixed> $messageAttributes
     * @throws MessageTooLargeException
     */
    public function __construct(
        private readonly string $message,
        private readonly array $messageAttributes,
        private readonly string $topicArn,
    ) {
        if ($this->size() > self::PAYLOAD_MAX_SIZE) {
            throw new MessageTooLargeException();
        }
    }

    /**
     * @return array<string,string|array<string,mixed>>
     */
    public function toArray(): array
    {
        return [
            'Message' => $this->message,
            'MessageAttributes' => $this->messageAttributes,
            'TopicArn' => $this->topicArn,
        ];
    }

    public function size(): int
    {
        return mb_strlen($this->message, '8bit');
    }
}
