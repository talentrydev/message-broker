<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sns;

use Talentry\MessageBroker\ApplicationInterface\Message\MessageTooLargeException;

class SnsBatchPayload
{
    public const int BATCH_MAX_ITEMS = 10;

    /**
     * @param SnsBatchPayloadEntry[] $entries
     * @throws MessageTooLargeException
     */
    public function __construct(
        private readonly string $topicArn,
        private readonly array $entries = []
    ) {
        if ($this->size() > SnsPayload::PAYLOAD_MAX_SIZE || count($this->entries) > self::BATCH_MAX_ITEMS) {
            throw new MessageTooLargeException();
        }
    }

    public function withEntry(SnsBatchPayloadEntry $entry): self
    {
        $entries = $this->entries;
        $entries[] = $entry;

        return new self($this->topicArn, $entries);
    }

    /**
     * @return array<string,string|array<array<string,string|array<string,mixed>>>>
     */
    public function toArray(): array
    {
        $entries = [];
        foreach ($this->entries as $entry) {
            $entries[] = $entry->toArray();
        }

        return [
            'PublishBatchRequestEntries' => $entries,
            'TopicArn' => $this->topicArn,
        ];
    }

    /**
     * @return string[]
     */
    public function getMessageIds(): array
    {
        $messageIds = [];
        foreach ($this->entries as $entry) {
            $messageIds[] = $entry->getMessageId();
        }

        return $messageIds;
    }

    public function size(): int
    {
        $size = 0;
        foreach ($this->entries as $entry) {
            $size += $entry->size();
        }

        return $size;
    }
}
