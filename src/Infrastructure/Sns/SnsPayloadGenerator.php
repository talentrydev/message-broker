<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sns;

use InvalidArgumentException;
use Predis\ClientInterface;
use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\Message\MessageTooLargeException;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageAttributesProvider;
use Generator;

class SnsPayloadGenerator
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly SqsMessageAttributesProvider $sqsMessageAttributesProvider,
        private readonly ChannelRegistry $channelRegistry,
        private readonly ?ClientInterface $redis = null,
    ) {
    }

    /**
     * @throws MessageTooLargeException
     */
    public function generateSnsPayload(Message $message, string $topicArn): SnsPayload
    {
        $serializationStrategy = $this->channelRegistry->getSerializationStrategy($message->getChannel());
        return new SnsPayload(
            message: $serializationStrategy->serialize($message),
            messageAttributes: $this->sqsMessageAttributesProvider->getMessageAttributes($message),
            topicArn: $topicArn,
        );
    }

    /**
     * @param Message[] $messages
     * @return Generator<int, SnsBatchPayload[]>
     */
    public function generateSnsBatchPayloads(array $messages, string $topicArn): Generator
    {
        foreach ($messages as $message) {
            if (!$message instanceof Message) {
                throw new InvalidArgumentException(
                    sprintf('Invalid type: %s provided, %s expected', gettype($message), Message::class)
                );
            }
        }

        $payloads = [];

        // make sure we don't generate more than 100 batches (100 * 10 = 1000 messages) at once or else
        // we will run out of memory sooner
        $payloadBatchLimit = 100;

        $payloadItemCount = 0;
        $payloadIndex = 0;
        $payloadSize = 0;
        foreach ($messages as $message) {
            $serializationStrategy = $this->channelRegistry->getSerializationStrategy($message->getChannel());
            $serializedMessage = $serializationStrategy->serialize($message);
            try {
                $messageSizeInBytes = (new SnsPayload($serializedMessage, [], $topicArn))->size();
            } catch (MessageTooLargeException) {
                $shortMessage = $message->getPayload();
                if (strlen($shortMessage) > 2_003) {
                    $shortMessage = substr_replace($shortMessage, '...', 1000, strlen($shortMessage) - 2000);
                }
                $this->logger->critical('SNS message too large. Discarding.', [
                    'channel' => $message->getChannelName(),
                    'payloadId' => $message->getPayloadId(),
                    'serializedMessage' => $shortMessage,
                ]);

                $this->redis?->setex(
                    'message-broker:message-too-large:' . $message->getPayloadId(),
                    86400,
                    $serializedMessage,
                );

                continue;
            }

            $payloadSize += $messageSizeInBytes;
            $payloadItemCount++;

            //overflow to the next payload index if we reach either item limit or size limit
            if ($payloadItemCount > SnsBatchPayload::BATCH_MAX_ITEMS || $payloadSize > SnsPayload::PAYLOAD_MAX_SIZE) {
                $payloadIndex++;
                $payloadItemCount = 1;
                $payloadSize = $messageSizeInBytes;
            }

            if (!array_key_exists($payloadIndex, $payloads)) {
                $payloads[$payloadIndex] = new SnsBatchPayload($topicArn);
            }

            $payloads[$payloadIndex] = $payloads[$payloadIndex]->withEntry(new SnsBatchPayloadEntry(
                messageId: $message->getId(),
                message: $serializedMessage,
                messageAttributes: $this->sqsMessageAttributesProvider->getMessageAttributes($message),
            ));

            if ($payloadIndex > $payloadBatchLimit) {
                yield $payloads;

                $payloadIndex = 0;
                $payloads = [];
            }
        }

        yield $payloads;
    }
}
