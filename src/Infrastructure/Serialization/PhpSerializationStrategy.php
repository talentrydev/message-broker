<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Serialization;

use DateTime;
use RuntimeException;
use Talentry\ErrorHandling\Error\Warning;
use Talentry\MessageBroker\ApplicationInterface\Message\Message as MessageInterface;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessage;

class PhpSerializationStrategy implements MessageSerializationStrategy
{
    public function serialize(MessageInterface $message): string
    {
        return serialize($message);
    }

    public function deserialize(string $data): MessageInterface
    {
        try {
            $message = unserialize($data, [
                'allowed_classes' => [
                    DateTime::class,
                    Channel::class,
                    Message::class,
                    SqsMessage::class,
                ]
            ]);
        } catch (Warning) {
            throw new RuntimeException('Failed to unserialize data');
        }

        if (!$message instanceof MessageInterface) {
            throw new RuntimeException('Failed to unserialize data');
        }

        return $message;
    }
}
