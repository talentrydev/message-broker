<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Serialization;

use RuntimeException;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;

class Base64PhpSerializationStrategy extends PhpSerializationStrategy
{
    public function serialize(Message $message): string
    {
        return base64_encode(parent::serialize($message));
    }

    public function deserialize(string $data): Message
    {
        $decoded = base64_decode(string: $data, strict: true);
        if ($decoded === false) {
            throw new RuntimeException('Failed to base64 decode data');
        }

        return parent::deserialize($decoded);
    }
}
