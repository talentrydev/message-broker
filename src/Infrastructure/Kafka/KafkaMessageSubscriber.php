<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Kafka;

use InvalidArgumentException;
use RdKafka\KafkaConsumer;
use RdKafka\Message as RdKafkaMessage;
use RuntimeException;
use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\ApplicationInterface\VisibilityTimeout\VisibilityTimeout;
use Talentry\MessageBroker\ApplicationInterface\WaitTime\WaitTime;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;

class KafkaMessageSubscriber implements MessageSubscriber
{
    private const int CONSUMER_TIMEOUT_MS = 10_000;

    /**
     * @var array<string,KafkaConsumer>
     */
    private array $kafkaConsumers = [];

    public function __construct(
        private readonly KafkaFactory $kafkaFactory,
        private readonly ChannelRegistry $channelRegistry,
        private readonly KafkaMessageMapper $kafkaMessageMapper = new KafkaMessageMapper(),
    ) {
    }

    public function __destruct()
    {
        foreach ($this->kafkaConsumers as $kafkaConsumer) {
            $kafkaConsumer->close();
        }
    }

    public function getMessageFromChannel(
        Channel $channel,
        bool $delete = true,
        ?WaitTime $waitTime = null,
        ?VisibilityTimeout $visibilityTimeout = null,
    ): ?Message {

        $consumer = $this->getKafkaConsumer($channel);
        $rdKafkaMessage = $consumer->consume(self::CONSUMER_TIMEOUT_MS);

        $serializationStrategy = $this->channelRegistry->getSerializationStrategy($channel);
        $message = $this->parseMessage($rdKafkaMessage, $serializationStrategy);
        if ($message !== null && $delete) {
            $this->deleteMessage($message, $channel);
        }

        return $message;
    }

    public function getAllMessagesFromChannel(
        Channel $channel,
        bool $delete = false,
    ): array {
        $serializationStrategy = $this->channelRegistry->getSerializationStrategy($channel);
        $consumer = $this->getKafkaConsumer($channel, createUniqueConsumerGroup: true);
        $messages = [];
        while ($rdKafkaMessage = $consumer->consume(self::CONSUMER_TIMEOUT_MS)) {
            $message = $this->parseMessage($rdKafkaMessage, $serializationStrategy);
            if ($message === null) {
                break;
            }

            if ($delete) {
                $this->deleteMessage($message, $channel);
            }

            $messages[] = $message;
        }

        return $messages;
    }

    public function deleteMessage(Message $message, Channel $channel): void
    {
        if (!$message instanceof KafkaMessage) {
            throw new InvalidArgumentException(sprintf('%s expected, got %s', KafkaMessage::class, $message::class));
        }

        if ($message->getRdKafkaMessage() === null) {
            throw new InvalidArgumentException('RdKafka\Message missing');
        }

        $this->getKafkaConsumer($channel)->commit($message->getRdKafkaMessage());
    }

    public function name(): string
    {
        return 'kafka';
    }

    private function getKafkaConsumer(Channel $channel, bool $createUniqueConsumerGroup = false): KafkaConsumer
    {
        if ($createUniqueConsumerGroup) {
            $consumer = $this->kafkaFactory->generateConsumer($channel->getName() . uniqid());
            $consumer->subscribe([$channel->getName()]);

            return $consumer;
        }

        if (!array_key_exists($channel->getName(), $this->kafkaConsumers)) {
            $consumer = $this->kafkaFactory->generateConsumer($channel->getName());
            $consumer->subscribe([$channel->getName()]);
            $this->kafkaConsumers[$channel->getName()] = $consumer;
        }

        return $this->kafkaConsumers[$channel->getName()];
    }

    private function parseMessage(
        RdKafkaMessage $rdKafkaMessage,
        MessageSerializationStrategy $serializationStrategy,
    ): ?KafkaMessage {
        if ($rdKafkaMessage->err === RD_KAFKA_RESP_ERR_NO_ERROR) {
            $message = $serializationStrategy->deserialize($rdKafkaMessage->payload);
            if (!$message instanceof KafkaMessage) {
                $message = $this->kafkaMessageMapper->toKafkaMessage($message);
            }

            $message->setRdKafkaMessage($rdKafkaMessage);

            return $message;
        } elseif ($rdKafkaMessage->err === RD_KAFKA_RESP_ERR__PARTITION_EOF) {
            return null;
        } else {
            throw new RuntimeException('Error fetching message from kafka: ' . $rdKafkaMessage->errstr());
        }
    }
}
