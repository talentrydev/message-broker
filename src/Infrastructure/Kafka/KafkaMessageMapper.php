<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Kafka;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;

class KafkaMessageMapper
{
    public function toKafkaMessage(Message $message): KafkaMessage
    {
        return new KafkaMessage(
            $message->getChannel(),
            $message->getPayload(),
            $message->getPayloadId(),
        );
    }
}
