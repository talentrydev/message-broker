<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Kafka;

use RdKafka\Conf;
use RdKafka\KafkaConsumer;
use RdKafka\Producer;

class KafkaFactory
{
    public function __construct(
        private readonly string $brokerList,
        private readonly string $autoOffsetReset = 'earliest',
        private readonly string $enablePartitionEof = 'true',
        private readonly string $enableAutoCommit = 'false',
        private readonly int $maxPollIntervalMs = 300_000,
        private readonly int $sessionTimeoutMs = 45_000,
        private readonly int $logLevel = LOG_ERR,
    ) {
    }

    public function generateProducer(): Producer
    {
        return new Producer($this->generateGenericConfiguration());
    }

    /**
     * @see https://arnaud.le-blanc.net/php-rdkafka-doc/phpdoc/rdkafka.examples-high-level-consumer.html
     */
    public function generateConsumer(string $consumerGroup): KafkaConsumer
    {
        $conf = $this->generateGenericConfiguration();
        // Set where to start consuming messages when there is no initial offset in
        // offset store or the desired offset is out of range.
        // 'earliest': start from the beginning
        $conf->set('auto.offset.reset', $this->autoOffsetReset);
        // Emit EOF event when reaching the end of a partition
        $conf->set('enable.partition.eof', $this->enablePartitionEof);
        // Configure the group.id. All consumer with the same group.id will consume
        // different partitions.
        $conf->set('group.id', $consumerGroup);
        // Disables auto-commit; offsets must be committed manually
        $conf->set('enable.auto.commit', $this->enableAutoCommit);
        $conf->set('max.poll.interval.ms', (string) $this->maxPollIntervalMs);
        $conf->set('session.timeout.ms', (string) $this->sessionTimeoutMs);

        return new KafkaConsumer($conf);
    }

    private function generateGenericConfiguration(): Conf
    {
        $conf = new Conf();
        $conf->set('metadata.broker.list', $this->brokerList);
        $conf->set('log_level', (string) $this->logLevel);

        return $conf;
    }
}
