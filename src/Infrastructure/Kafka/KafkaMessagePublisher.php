<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Kafka;

use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RdKafka\Producer;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\VoidFailedMessageSpool;
use Talentry\MessageBroker\Infrastructure\Logging\LoggerTrait;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;

class KafkaMessagePublisher implements MessagePublisher
{
    use LoggerTrait;

    private const int FLUSH_TIMEOUT_MS = 10_000;

    private Producer $producer;

    public function __construct(
        private readonly KafkaFactory $kafkaFactory,
        private readonly ChannelRegistry $channelRegistry,
        private readonly FailedMessageSpool $failedMessageSpool = new VoidFailedMessageSpool(),
        private readonly LoggerInterface $logger = new NullLogger(),
    ) {
        $this->producer = $this->kafkaFactory->generateProducer();
    }

    public function __destruct()
    {
        //@see https://github.com/arnaud-lb/php-rdkafka#proper-shutdown
        $this->producer->flush(self::FLUSH_TIMEOUT_MS);
    }

    public function sendMessage(Message $message): void
    {
        try {
            $serializationStrategy = $this->channelRegistry->getSerializationStrategy($message->getChannel());
            $topic = $this->producer->newTopic($message->getChannel()->getName());
            $topic->produce(RD_KAFKA_PARTITION_UA, 0, $serializationStrategy->serialize($message));
            $this->logSendMessage($message);
        } catch (Exception $e) {
            $this->getFailedMessageSpool()->push($message);
            $this->logPushToFailedMessageSpool($message, $e->getMessage());
        }
    }

    public function getFailedMessageSpool(): FailedMessageSpool
    {
        return $this->failedMessageSpool;
    }

    public function name(): string
    {
        return 'kafka';
    }

    public function batchSendMessages(array $messages): void
    {
        foreach ($messages as $message) {
            $this->sendMessage($message);
        }
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}
