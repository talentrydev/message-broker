<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Kafka;

use Talentry\MessageBroker\Domain\Message\Message;
use RdKafka\Message as RdKafkaMessage;

class KafkaMessage extends Message
{
    private ?RdKafkaMessage $rdKafkaMessage = null;

    public function getRdKafkaMessage(): ?RdKafkaMessage
    {
        return $this->rdKafkaMessage;
    }

    public function setRdKafkaMessage(RdKafkaMessage $rdKafkaMessage): void
    {
        $this->rdKafkaMessage = $rdKafkaMessage;
    }
}
