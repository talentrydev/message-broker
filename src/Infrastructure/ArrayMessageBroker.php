<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure;

use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\ApplicationInterface\VisibilityTimeout\VisibilityTimeout as VTInterface;
use Talentry\MessageBroker\ApplicationInterface\WaitTime\WaitTime;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\VoidFailedMessageSpool;
use Talentry\MessageBroker\Infrastructure\Logging\LoggerTrait;

/**
 * This bus has the ability to put a limit on the number of events it will dispatch before it pretends to be empty.
 * This allows you to process one event at a time and see the effect after each.
 */
class ArrayMessageBroker implements MessagePublisher, MessageSubscriber
{
    use LoggerTrait;

    /**
     * @var array<string,Message[]>
     */
    private array $messages = [];

    public function __construct(
        private readonly LoggerInterface $logger = new NullLogger(),
    ) {
    }

    public function sendMessage(Message $message): void
    {
        $this->enqueueMessage($message);
        $this->logSendMessage($message);
    }

    /**
     * @param Message[] $messages
     */
    public function batchSendMessages(array $messages): void
    {
        foreach ($messages as $message) {
            if (!$message instanceof Message) {
                throw new InvalidArgumentException(
                    sprintf('Invalid type: %s provided, %s expected', gettype($message), Message::class)
                );
            }
        }

        foreach ($messages as $message) {
            $this->sendMessage($message);
        }
    }

    public function getMessageFromChannel(
        Channel $channel,
        bool $delete = true,
        ?WaitTime $waitTime = null,
        ?VTInterface $visibilityTimeout = null,
    ): ?Message {
        $key = $channel->getName();

        if (!array_key_exists($key, $this->messages)) {
            return null;
        }

        $message = array_shift($this->messages[$key]);

        if ($message === null) {
            return null;
        }

        $this->logGetMessage($channel, $exists = true);

        return $message;
    }

    public function getAllMessagesFromChannel(
        Channel $channel,
        bool $delete = false,
    ): array {
        $key = $channel->getName();

        $messages = $this->messages[$key] ?? [];

        if ($delete) {
            $this->messages[$key] = [];
        }

        return $messages;
    }

    /**
     * Look at all of the event payloads, assuming the payloads are PHP serialized objects.
     *
     * @return mixed[]|null
     */
    public function getUnserialized(Channel $channel): ?array
    {
        $key = $channel->getName();
        if (!array_key_exists($key, $this->messages)) {
            return null;
        }

        return array_values(array_map(
            static fn (Message $message) => unserialize(json_decode($message->getPayload(), true)[0]),
            $this->messages[$key],
        ));
    }

    public function deleteMessage(Message $message, Channel $channel): void
    {
        $channel = $channel->getName();
        if (!isset($this->messages[$channel])) {
            return;
        }

        foreach ($this->messages[$channel] as $key => $existingMessage) {
            if ($existingMessage->getId() === $message->getId()) {
                unset($this->messages[$channel][$key]);
                return;
            }
        }
    }

    private function enqueueMessage(Message $message): void
    {
        $channel = $message->getChannel()->getName();
        if (!isset($this->messages[$channel])) {
            $this->messages[$channel] = [];
        }

        $this->messages[$channel][] = $message;
    }

    /**
     * For resetting between tests.
     */
    public function reset(): void
    {
        $this->messages = [];
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function name(): string
    {
        return 'array';
    }

    public function getFailedMessageSpool(): FailedMessageSpool
    {
        return new VoidFailedMessageSpool();
    }
}
