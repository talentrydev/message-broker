<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure;

use Aws\Middleware;
use Aws\RetryMiddleware;
use Aws\Sns\SnsClient;
use Aws\Sqs\SqsClient;
use InvalidArgumentException;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Cache\Adapter\NullAdapter;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaFactory;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Kafka\KafkaMessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\Sns\SnsMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Sns\SnsPayloadGenerator;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageAttributesProvider;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageMapper;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessagePublisher;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsMessageSubscriber;
use Talentry\MessageBroker\Infrastructure\Sqs\SqsQueueFactory;

class MessageBrokerFactory
{
    private const string AWS_SQS_VERSION = '2012-11-05';
    private const string AWS_SNS_VERSION = '2010-03-31';
    private const string DEFAULT_AWS_REGION = 'eu-central-1';

    private ArrayMessageBroker $arrayMessageBroker;

    public function __construct(
        private readonly FailedMessageSpool $failedMessageSpool,
        private readonly SnsPayloadGenerator $snsPayloadGenerator,
        private readonly SqsMessageAttributesProvider $sqsMessageAttributesProvider,
        private readonly ChannelRegistry $channelRegistry,
        private readonly LoggerInterface $logger,
        private readonly ?string $publisherImplementation = null,
        private readonly ?string $subscriberImplementation = null,
        private readonly ?string $namespace = null,
        private readonly ?string $awsRegion = null,
        private readonly ?string $snsTopicArn = null,
        private readonly ?string $snsEndpoint = null,
        private readonly ?string $sqsEndpoint = null,
        private readonly ?string $kafkaBrokerList = null,
        private readonly ?string $kafkaAutoOffsetReset = null,
        private readonly ?string $kafkaEnablePartitionEof = null,
        private readonly ?string $kafkaEnableAutoCommit = null,
        private readonly ?int $kafkaLogLevel = null,
        private readonly ?int $kafkaMaxPollIntervalMs = null,
        private readonly ?int $kafkaSessionTimeoutMs = null,
        private readonly CacheItemPoolInterface $sqsQueuesCache = new NullAdapter(),
    ) {
    }

    public function generateMessagePublisher(): MessagePublisher
    {
        return match ($this->publisherImplementation) {
            'sns' =>  $this->generateSnsMessagePublisher(),
            'sqs' =>  $this->generateSqsMessagePublisher(),
            'kafka' =>  $this->generateKafkaMessagePublisher(),
            'array' =>  $this->generateArrayMessageBroker(),
            default => throw new InvalidArgumentException(
                'Unsupported message publisher: ' . $this->publisherImplementation,
            ),
        };
    }

    public function generateMessageSubscriber(): MessageSubscriber
    {
        return match ($this->subscriberImplementation) {
            'sqs' =>  $this->generateSqsMessageSubscriber(),
            'kafka' =>  $this->generateKafkaMessageSubscriber(),
            'array' =>  $this->generateArrayMessageBroker(),
            default => throw new InvalidArgumentException(
                'Unsupported message subscriber: ' . $this->subscriberImplementation,
            ),
        };
    }

    private function generateSnsMessagePublisher(): SnsMessagePublisher
    {
        if ($this->snsTopicArn === null) {
            throw new RuntimeException('SNS topic ARN must be provided when using SNS publisher implementation');
        }

        $snsClient = new SnsClient([
            'region' => $this->awsRegion ?? self::DEFAULT_AWS_REGION,
            'version' => self::AWS_SNS_VERSION,
            'endpoint' => $this->snsEndpoint,
        ]);

        $decider = RetryMiddleware::createDefaultDecider(
            extraConfig: [
                'curl_errors' => [CURLE_SSL_CONNECT_ERROR]
            ]
        );

        $snsClient
            ->getHandlerList()
            ->appendSign(
                Middleware::retry($decider),
                'retry'
            );

        return new SnsMessagePublisher(
            $snsClient,
            $this->failedMessageSpool,
            $this->snsPayloadGenerator,
            $this->logger,
            $this->snsTopicArn,
        );
    }

    private function generateSqsMessagePublisher(): SqsMessagePublisher
    {
        $sqsClient = $this->generateSqsClient();
        $sqsQueueFactory = $this->generateSqsQueueFactory($sqsClient);

        return new SqsMessagePublisher(
            $sqsClient,
            new SqsMessageMapper(),
            $this->sqsMessageAttributesProvider,
            $sqsQueueFactory,
            $this->failedMessageSpool,
            $this->logger,
            $this->channelRegistry,
        );
    }

    private function generateKafkaMessagePublisher(): KafkaMessagePublisher
    {
        if ($this->kafkaBrokerList === null) {
            throw new RuntimeException('Kafka broker list must be provided when using kafka publisher implementation');
        }

        return new KafkaMessagePublisher(
            $this->generateKafkaFactory(),
            $this->channelRegistry,
            $this->failedMessageSpool,
            $this->logger,
        );
    }

    private function generateSqsMessageSubscriber(): SqsMessageSubscriber
    {
        $sqsClient = $this->generateSqsClient();
        $sqsQueueFactory = $this->generateSqsQueueFactory($sqsClient);

        return new SqsMessageSubscriber(
            $sqsClient,
            $sqsQueueFactory,
            $this->logger,
            new SqsMessageMapper(),
            $this->channelRegistry,
        );
    }

    private function generateKafkaMessageSubscriber(): KafkaMessageSubscriber
    {
        if ($this->kafkaBrokerList === null) {
            throw new RuntimeException('Kafka broker list must be provided when using kafka subscriber implementation');
        }

        return new KafkaMessageSubscriber(
            $this->generateKafkaFactory(),
            $this->channelRegistry,
        );
    }

    private function generateArrayMessageBroker(): ArrayMessageBroker
    {
        if (!isset($this->arrayMessageBroker)) {
            $this->arrayMessageBroker = new ArrayMessageBroker(logger: $this->logger);
        }

        return $this->arrayMessageBroker;
    }

    private function generateSqsClient(): SqsClient
    {
        return new SqsClient([
            'region' => $this->awsRegion ?? self::DEFAULT_AWS_REGION,
            'version' => self::AWS_SQS_VERSION,
            'endpoint' => $this->sqsEndpoint,
        ]);
    }

    private function generateSqsQueueFactory(SqsClient $sqsClient): SqsQueueFactory
    {
        return new SqsQueueFactory(
            $sqsClient,
            $this->channelRegistry,
            $this->sqsQueuesCache,
            $this->namespace,
        );
    }

    private function generateKafkaFactory(): KafkaFactory
    {
        return new KafkaFactory(
            $this->kafkaBrokerList,
            $this->kafkaAutoOffsetReset ?? 'earliest',
            $this->kafkaEnablePartitionEof ?? 'true',
            $this->kafkaEnableAutoCommit ?? 'false',
            $this->kafkaMaxPollIntervalMs ?? 300_000,
            $this->kafkaSessionTimeoutMs ?? 45_000,
            $this->kafkaLogLevel ?? LOG_ERR,
        );
    }
}
