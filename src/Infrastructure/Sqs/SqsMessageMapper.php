<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sqs;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;

class SqsMessageMapper
{
    public function toSqsMessage(Message $message): SqsMessage
    {
        return new SqsMessage(
            $message->getChannel(),
            $message->getPayload(),
            $message->getPayloadId(),
        );
    }
}
