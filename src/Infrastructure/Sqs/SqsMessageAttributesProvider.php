<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sqs;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\Domain\Exception\UnsupportedMessageException;

class SqsMessageAttributesProvider
{
    public function __construct(
        private readonly MessageParser $messageParser,
    ) {
    }

    /**
     * @return array<string,array<string,string>>
     */
    public function getMessageAttributes(Message $message): array
    {
        return [
            'MessageId' => [
                'DataType' => 'String',
                'StringValue' => $message->getId()
            ],
            'TenantId' => [
                'DataType' => 'String',
                'StringValue' => $this->extractTenantId($message)
            ],
            'MessageType' => [
                'DataType' => 'String',
                'StringValue' => addslashes($this->extractMessageType($message)),
            ],
            'Channel' => [
                'DataType' => 'String',
                'StringValue' => $message->getChannel()->getName(),
            ]
        ];
    }

    private function extractTenantId(Message $message): string
    {
        try {
            return $this->messageParser->getTenantId($message) ?? 'N/A';
        } catch (UnsupportedMessageException) {
            return 'N/A';
        }
    }

    private function extractMessageType(Message $message): string
    {
        try {
            return $this->messageParser->getMessageType($message);
        } catch (UnsupportedMessageException) {
            return 'N/A';
        }
    }
}
