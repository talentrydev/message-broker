<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sqs;

use Aws\Exception\AwsException;
use Aws\Sqs\SqsClient;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\Infrastructure\Logging\LoggerTrait;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;

class SqsMessagePublisher implements MessagePublisher
{
    use LoggerTrait;

    public function __construct(
        private readonly SqsClient $sqsClient,
        private readonly SqsMessageMapper $messageMapper,
        private readonly SqsMessageAttributesProvider $sqsMessageAttributesProvider,
        private readonly SqsQueueFactory $sqsQueueFactory,
        private readonly FailedMessageSpool $failedMessageSpool,
        private readonly LoggerInterface $logger,
        private readonly ChannelRegistry $channelRegistry,
    ) {
    }

    public function sendMessage(Message $message): void
    {
        $serializationStrategy = $this->channelRegistry->getSerializationStrategy($message->getChannel());

        $message = $this->messageMapper->toSqsMessage($message);

        $sqsQueue = $this->sqsQueueFactory->fromChannel($message->getChannel());

        $attributes = [
            'QueueUrl' => $sqsQueue->url(),
            'MessageBody' => $serializationStrategy->serialize($message),
            'MessageAttributes' => $this->sqsMessageAttributesProvider->getMessageAttributes($message),
        ];

        try {
            $this->sendAndLogMessage($attributes, $message);
        } catch (AwsException $e) {
            $this->getFailedMessageSpool()->push($message);
            $this->logPushToFailedMessageSpool($message, $e->getAwsErrorMessage() ?? 'AwsException');
        } catch (Exception $e) {
            $this->getFailedMessageSpool()->push($message);
            $this->logPushToFailedMessageSpool($message, $e->getMessage());
        }
    }

    /**
     * @param Message[] $messages
     */
    public function batchSendMessages(array $messages): void
    {
        foreach ($messages as $message) {
            if (!$message instanceof Message) {
                throw new InvalidArgumentException(
                    sprintf('Invalid type: %s provided, %s expected', gettype($message), Message::class)
                );
            }
        }

        foreach ($messages as $message) {
            $this->sendMessage($message);
        }
    }

    /**
     * @param array<string,mixed> $attributes
     */
    private function sendAndLogMessage(array $attributes, SqsMessage $message): void
    {
        $this->sqsClient->sendMessage($attributes);
        $this->logSendMessage($message);
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function name(): string
    {
        return 'sqs';
    }

    public function getFailedMessageSpool(): FailedMessageSpool
    {
        return $this->failedMessageSpool;
    }
}
