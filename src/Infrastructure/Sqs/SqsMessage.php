<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sqs;

use Talentry\MessageBroker\Domain\Message\Message;

class SqsMessage extends Message
{
    private ?string $receiptHandle = null;

    public function getReceiptHandle(): ?string
    {
        return $this->receiptHandle;
    }

    public function setReceiptHandle(string $receiptHandle): void
    {
        $this->receiptHandle = $receiptHandle;
    }
}
