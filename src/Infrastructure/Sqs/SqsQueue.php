<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sqs;

use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use Exception;
use Psr\Cache\CacheItemPoolInterface;
use RuntimeException;
use Talentry\MessageBroker\Domain\Channel\RetentionPeriod;
use Talentry\MessageBroker\Domain\Exception\CreateChannelException;

class SqsQueue
{
    /**
     * These limits are enforced by AWS
     * https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_SetQueueAttributes.html
     */
    private const int MINIMUM_RETENTION_PERIOD = 60;
    private const int MAXIMUM_RETENTION_PERIOD = 14 * 86400;

    private ?string $url = null;
    private ?string $arn = null;

    /**
     * @throws CreateChannelException
     */
    public function __construct(
        private readonly SqsClient $sqsClient,
        private readonly CacheItemPoolInterface $cachePool,
        private string $name,
        ?string $namespace = null,
        private readonly ?RetentionPeriod $retentionPeriod = null,
        private readonly ?RedrivePolicy $redrivePolicy = null,
    ) {
        if ($namespace !== null) {
            $this->name = $namespace . '_' . $this->name;
        }

        $this->initialize();
    }

    public function id(): string
    {
        return $this->arn();
    }

    public function url(): string
    {
        if ($this->url === null) {
            throw new RuntimeException('SqsQueue was not properly initialized: queue URL missing');
        }
        return $this->url;
    }

    public function arn(): string
    {
        if ($this->arn === null) {
            throw new RuntimeException('SqsQueue was not properly initialized: queue ARN missing');
        }
        return $this->arn;
    }

    /**
     * @throws CreateChannelException
     */
    private function initialize(): void
    {
        if ($this->restoreFromCache()) {
            return;
        }

        if (!$this->restoreFromExistingQueue()) {
            $this->createQueue();
        }

        $this->storeToCache();
    }

    /**
     * @throws CreateChannelException
     */
    public function createQueue(): void
    {
        try {
            $options = [
                'QueueName' => $this->name
            ];
            $attributes = [];
            if ($this->retentionPeriod !== null) {
                //AWS SDK triggers a warning when we supply an integer value for MessageRetentionPeriod;
                //this is clearly wrong, but the SDK will cast to string anyway,
                //so we might as well do it here and prevent the warning from being triggered.
                //@see https://github.com/aws/aws-sdk-php/issues/2826
                $attributes['MessageRetentionPeriod'] = (string) $this->retentionPeriod->asInteger(
                    self::MINIMUM_RETENTION_PERIOD,
                    self::MAXIMUM_RETENTION_PERIOD,
                );
            }
            if ($this->redrivePolicy !== null) {
                $attributes['RedrivePolicy'] = json_encode([
                    'deadLetterTargetArn' => $this->redrivePolicy->deadLetterQueue()->id(),
                    'maxReceiveCount' => $this->redrivePolicy->maxReceiveCount(),
                ]);
            }

            if (count($attributes) > 0) {
                $options['Attributes'] = $attributes;
            }

            $result = $this->sqsClient->createQueue($options);
            $this->url = (string) $result->get('QueueUrl');
            $this->arn = $this->fetchQueueArn();
        } catch (Exception $e) {
            throw new CreateChannelException($this->name, $e);
        }
    }

    private function restoreFromCache(): bool
    {
        try {
            $cacheItem = $this->cachePool->getItem($this->name);
            if ($cacheItem->isHit()) {
                $data = $cacheItem->get();
                if (is_array($data) && isset($data['url']) && isset($data['arn'])) {
                    $this->url = $data['url'];
                    $this->arn = $data['arn'];

                    return true;
                }
            }

            return false;
        } catch (Exception) {
            return false;
        }
    }

    private function storeToCache(): void
    {
        try {
            $cacheItem = $this->cachePool->getItem($this->name);
            $cacheItem->set(['url' => $this->url(), 'arn' => $this->arn()]);
            $this->cachePool->save($cacheItem);
        } catch (Exception) {
            // cache is allowed to fail
        }
    }

    private function restoreFromExistingQueue(): bool
    {
        try {
            $result = $this->sqsClient->getQueueUrl(['QueueName' => $this->name]);
            $this->url = $result['QueueUrl'];
            $this->arn = $this->fetchQueueArn();

            return true;
        } catch (SqsException) {
            return false;
        }
    }

    private function fetchQueueArn(): string
    {
        $result = $this->sqsClient->getQueueAttributes([
            'QueueUrl' => $this->url(),
            'AttributeNames' => ['QueueArn']
        ]);

        return $result['Attributes']['QueueArn'];
    }
}
