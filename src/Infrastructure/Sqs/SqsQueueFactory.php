<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sqs;

use Aws\Sqs\SqsClient;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Cache\Adapter\NullAdapter;
use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Domain\Channel\RetentionPeriod;
use Talentry\MessageBroker\Domain\Exception\CreateChannelException;

class SqsQueueFactory
{
    private const int MAX_RECEIVE_COUNT = 1;

    /**
     * Name of the CacheItemPoolInterface must match the name of the cache pool defined in symfony framework config,
     * except for the fact that name in config uses snake case, and here, camel case is used.
     */
    public function __construct(
        private readonly SqsClient $sqsClient,
        private readonly ChannelRegistry $channelRegistry,
        private readonly CacheItemPoolInterface $sqsQueuesCache = new NullAdapter(),
        private readonly ?string $namespace = null,
    ) {
    }

    /**
     * @throws CreateChannelException
     */
    public function fromChannel(Channel $channel): SqsQueue
    {
        $redrivePolicy = null;
        if (!$this->channelRegistry->isDeadLetterQueue($channel)) {
            $redrivePolicy = new RedrivePolicy($this->createDeadLetterQueue($channel), self::MAX_RECEIVE_COUNT);
        }

        return new SqsQueue(
            $this->sqsClient,
            $this->sqsQueuesCache,
            $channel->getName(),
            $this->namespace,
            null,
            $redrivePolicy
        );
    }

    /**
     * @throws CreateChannelException
     */
    public function createDeadLetterQueue(Channel $channel): SqsQueue
    {
        return new SqsQueue(
            $this->sqsClient,
            $this->sqsQueuesCache,
            $this->channelRegistry->getDeadLetterQueueNameForChannel($channel),
            $this->namespace,
            RetentionPeriod::maximum()
        );
    }
}
