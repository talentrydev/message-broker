<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sqs;

/**
 * Redrive policy determines the behavior for undeliverable messages.
 * These may be sent to a dead letter queue, after a specified number of retries (maxReceiveCount).
 */
class RedrivePolicy
{
    public function __construct(
        private readonly SqsQueue $deadLetterQueue,
        private readonly int $maxReceiveCount,
    ) {
    }

    public function deadLetterQueue(): SqsQueue
    {
        return $this->deadLetterQueue;
    }

    public function maxReceiveCount(): int
    {
        return $this->maxReceiveCount;
    }
}
