<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Infrastructure\Sqs;

use Aws\Sqs\SqsClient;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\ApplicationInterface\VisibilityTimeout\VisibilityTimeout as VisibilityTimeoutInterface;
use Talentry\MessageBroker\ApplicationInterface\WaitTime\WaitTime as WaitTimeInterface;
use Talentry\MessageBroker\Domain\Exception\GetMessageException;
use Talentry\MessageBroker\Domain\Message\VisibilityTimeout;
use Talentry\MessageBroker\Domain\Message\WaitTime;
use Talentry\MessageBroker\Infrastructure\Logging\LoggerTrait;
use Talentry\MessageBroker\Infrastructure\MessageSubscriber\MessageSubscriberTrait;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;

class SqsMessageSubscriber implements MessageSubscriber
{
    use LoggerTrait;
    use MessageSubscriberTrait;

    private const int MAX_WAIT_TIME = 20;
    private const int MAX_VISIBILITY_TIMEOUT = 12 * 3600;

    public function __construct(
        private readonly SqsClient $sqsClient,
        private readonly SqsQueueFactory $sqsQueueFactory,
        private readonly LoggerInterface $logger,
        private readonly SqsMessageMapper $sqsMessageMapper,
        private readonly ChannelRegistry $channelRegistry,
    ) {
    }

    public function getMessageFromChannel(
        Channel $channel,
        bool $delete = true,
        ?WaitTimeInterface $waitTime = null,
        ?VisibilityTimeoutInterface $visibilityTimeout = null,
    ): ?Message {
        $sqsQueue = $this->sqsQueueFactory->fromChannel($channel);

        $serializationStrategy = $this->channelRegistry->getSerializationStrategy($channel);
        $message = $this->getMessageFromClient($sqsQueue, $serializationStrategy, $waitTime, $visibilityTimeout);
        if ($message === null) {
            $this->logGetMessage($channel, false);

            return null;
        }

        $this->logGetMessage($channel, true);

        if ($delete) {
            $this->deleteMessage($message, $channel);
        }

        return $message;
    }

    public function deleteMessage(Message $message, Channel $channel): void
    {
        if (!$message instanceof SqsMessage) {
            throw new InvalidArgumentException(sprintf('%s expected, got %s', SqsMessage::class, $message::class));
        }

        if ($message->getReceiptHandle() === null) {
            throw new InvalidArgumentException();
        }

        $this->sqsClient->deleteMessage([
            'QueueUrl' => $this->sqsQueueFactory->fromChannel($channel)->url(),
            'ReceiptHandle' => $message->getReceiptHandle(),
        ]);
    }

    private function getMessageFromClient(
        SqsQueue $sqsQueue,
        MessageSerializationStrategy $serializationStrategy,
        ?WaitTimeInterface $waitTime = null,
        ?VisibilityTimeoutInterface $visibilityTimeout = null,
    ): ?Message {
        if ($waitTime === null) {
            $waitTime = WaitTime::defaultValue();
        }

        if ($visibilityTimeout === null) {
            $visibilityTimeout = VisibilityTimeout::defaultValue();
        }

        $result = $this->sqsClient->receiveMessage(
            [
                'QueueUrl' => $sqsQueue->url(),
                'MaxNumberOfMessages' => 1,
                'WaitTimeSeconds' => $waitTime->getWaitTime(self::MAX_WAIT_TIME),
                'VisibilityTimeout' => $visibilityTimeout->getVisibilityTimeout(self::MAX_VISIBILITY_TIMEOUT),
            ]
        );

        $messages = $result->get('Messages');
        if (!is_array($messages) || count($messages) === 0) {
            return null;
        }

        $messageData = array_shift($messages);
        if (!is_array($messageData) || !isset($messageData['Body']) || !isset($messageData['ReceiptHandle'])) {
            throw new GetMessageException();
        }

        $message = $serializationStrategy->deserialize($messageData['Body']);
        if (!$message instanceof SqsMessage) {
            $message = $this->sqsMessageMapper->toSqsMessage($message);
        }

        $message->setReceiptHandle((string) $messageData['ReceiptHandle']);

        return $message;
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function name(): string
    {
        return 'sqs';
    }

    public function getMaximumVisibilityTimeout(): int
    {
        return self::MAX_VISIBILITY_TIMEOUT;
    }
}
