<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Presentation\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;

#[AsCommand(
    name: 'ty:message-broker:failed-message-spool-processor',
    description: 'Takes messages from the failed message spool and publishes them using message broker',
)]
class FailedMessageSpoolProcessorCommand extends Command
{
    public function __construct(
        private readonly MessagePublisher $messagePublisher,
        private readonly LoggerInterface $logger
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        while ($message = $this->messagePublisher->getFailedMessageSpool()->pop()) {
            $this->messagePublisher->sendMessage($message);

            $this->logger->info(sprintf(
                'Message %s:%s pushed from failed message spool to message broker',
                $message->getChannel()->getName(),
                $message->getPayloadId()
            ));
        }

        return self::SUCCESS;
    }
}
