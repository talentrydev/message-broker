<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Channel;

class RetentionPeriod
{
    private const int MINIMUM_RETENTION_PERIOD = -1;
    private const int MAXIMUM_RETENTION_PERIOD = -2;

    private function __construct(
        private readonly int $retentionPeriod,
    ) {
    }

    public static function minimum(): self
    {
        return new self(self::MINIMUM_RETENTION_PERIOD);
    }

    public static function maximum(): self
    {
        return new self(self::MAXIMUM_RETENTION_PERIOD);
    }

    public static function days(int $days): self
    {
        return new self($days * 86400);
    }

    public function asInteger(int $minimumValue, int $maximumValue): int
    {
        $retentionPeriod = match ($this->retentionPeriod) {
            self::MINIMUM_RETENTION_PERIOD => $minimumValue,
            self::MAXIMUM_RETENTION_PERIOD => $maximumValue,
            default => $this->retentionPeriod
        };

        if ($retentionPeriod > $maximumValue) {
            $retentionPeriod = $maximumValue;
        }

        if ($retentionPeriod < $minimumValue) {
            $retentionPeriod = $minimumValue;
        }

        return $retentionPeriod;
    }
}
