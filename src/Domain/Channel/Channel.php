<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Channel;

use Talentry\MessageBroker\ApplicationInterface\Channel\Channel as ChannelInterface;

class Channel implements ChannelInterface
{
    public function __construct(
        private readonly string $name,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }
}
