<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Channel;

use Talentry\MessageBroker\ApplicationInterface\Channel\Channel as ChannelInterface;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\Infrastructure\Serialization\Base64PhpSerializationStrategy;

class ChannelRegistry
{
    /**
     * @var Channel[]
     */
    private array $channels = [];

    /**
     * @var array<string, MessageSerializationStrategy>
     */
    private array $serializationStrategies = [];

    public function __construct(
        private readonly string $deadLetterQueueSuffix = '_DLQ',
    ) {
    }

    public function registerChannel(
        string|Channel $channel,
        MessageSerializationStrategy $serializationStrategy = new Base64PhpSerializationStrategy(),
    ): void {
        if (!$channel instanceof Channel) {
            $channel = new Channel($channel);
        }

        $this->channels[] = $channel;
        $this->serializationStrategies[$channel->getName()] = $serializationStrategy;
        $this->serializationStrategies[$this->getDeadLetterQueueNameForChannel($channel)] = $serializationStrategy;
    }

    /**
     * @return Channel[]
     */
    public function getChannels(): array
    {
        return $this->channels;
    }

    public function getSerializationStrategy(Channel $channel): MessageSerializationStrategy
    {
        if (!array_key_exists($channel->getName(), $this->serializationStrategies)) {
            return new Base64PhpSerializationStrategy(); //fallback
        }

        return $this->serializationStrategies[$channel->getName()];
    }

    /**
     * @return string[]
     */
    public function getAllDeadLetterQueueNames(): array
    {
        $dlqNames = [];
        foreach ($this->getChannels() as $channel) {
            $dlqNames[] = $this->getDeadLetterQueueNameForChannel($channel);
        }

        return $dlqNames;
    }

    public function getDeadLetterQueueNameForChannel(ChannelInterface $channel): string
    {
        return $channel->getName() . $this->deadLetterQueueSuffix;
    }

    public function isDeadLetterQueue(ChannelInterface $channel): bool
    {
        return str_ends_with($channel->getName(), $this->deadLetterQueueSuffix);
    }
}
