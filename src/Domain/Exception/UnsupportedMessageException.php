<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Exception;

use Talentry\MessageBroker\ApplicationInterface\Message\UnsupportedMessageException as ExceptionInterface;

class UnsupportedMessageException extends MessageBrokerException implements ExceptionInterface
{
}
