<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Exception;

use Exception;

class MessageBrokerException extends Exception
{
}
