<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Exception;

class GetMessageException extends MessageBrokerException
{
}
