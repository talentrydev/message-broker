<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Exception;

use Throwable;

class CreateChannelException extends MessageBrokerException
{
    public function __construct(string $channelName, ?Throwable $causedBy = null)
    {
        parent::__construct('Error creating channel: ' . $channelName, 0, $causedBy);
    }
}
