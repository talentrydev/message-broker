<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Message;

use InvalidArgumentException;
use Talentry\MessageBroker\ApplicationInterface\VisibilityTimeout\VisibilityTimeout as VisibilityTimeoutInterface;

class VisibilityTimeout implements VisibilityTimeoutInterface
{
    private const int MAXIMUM_VISIBILITY_TIMEOUT = -1;
    private const int MINIMUM_VISIBILITY_TIMEOUT = 0;
    private const int DEFAULT_VISIBILITY_TIMEOUT = 30 * 60;

    private function __construct(
        private readonly int $visibilityTimeout,
    ) {
    }

    public static function inSeconds(int $seconds): self
    {
        if ($seconds < 0) {
            throw new InvalidArgumentException('Visibility timeout must not be negative');
        }

        return new self($seconds);
    }

    public static function maximumValue(): self
    {
        return new self(self::MAXIMUM_VISIBILITY_TIMEOUT);
    }

    public static function minimumValue(): self
    {
        return new self(self::MINIMUM_VISIBILITY_TIMEOUT);
    }

    public static function defaultValue(): self
    {
        return new self(self::DEFAULT_VISIBILITY_TIMEOUT);
    }

    public function getVisibilityTimeout(int $maximumVisibilityTimeout): int
    {
        if ($this->visibilityTimeout === self::MAXIMUM_VISIBILITY_TIMEOUT) {
            return $maximumVisibilityTimeout;
        }

        if ($this->visibilityTimeout > $maximumVisibilityTimeout) {
            throw new InvalidArgumentException('Provided visibility timeout exceeds maximum value');
        }

        return $this->visibilityTimeout;
    }
}
