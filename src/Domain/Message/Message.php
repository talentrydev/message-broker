<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Message;

use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\Message\Message as MessageInterface;

class Message implements MessageInterface
{
    public function __construct(
        private readonly Channel $channel,
        private readonly string $payload,
        private readonly string $payloadId,
    ) {
    }

    public function getPayload(): string
    {
        return $this->payload;
    }

    public function getChannelName(): string
    {
        return $this->channel->getName();
    }

    public function getChannel(): Channel
    {
        return $this->channel;
    }

    public function getPayloadId(): string
    {
        return $this->payloadId;
    }

    public function getId(): string
    {
        return sprintf(
            '%s_%s',
            $this->getChannelName(),
            $this->getPayloadId()
        );
    }
}
