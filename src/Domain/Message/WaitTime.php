<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\Message;

use InvalidArgumentException;
use Talentry\MessageBroker\ApplicationInterface\WaitTime\WaitTime as WaitTimeInterface;

class WaitTime implements WaitTimeInterface
{
    private const int MAXIMUM_WAIT_TIME = -1;
    private const int MINIMUM_WAIT_TIME = 0;

    private function __construct(
        private readonly int $waitTime,
    ) {
    }

    public static function inSeconds(int $seconds): self
    {
        if ($seconds < 0) {
            throw new InvalidArgumentException('Wait time must not be negative');
        }

        return new self($seconds);
    }

    public static function maximumValue(): self
    {
        return new self(self::MAXIMUM_WAIT_TIME);
    }

    public static function minimumValue(): self
    {
        return new self(self::MINIMUM_WAIT_TIME);
    }

    public static function defaultValue(): self
    {
        return self::minimumValue();
    }

    public function getWaitTime(int $maximumWaitTime): int
    {
        if ($this->waitTime === self::MAXIMUM_WAIT_TIME) {
            return $maximumWaitTime;
        }

        if ($this->waitTime > $maximumWaitTime) {
            throw new InvalidArgumentException('Provided wait time exceeds maximum wait time');
        }

        return $this->waitTime;
    }
}
