<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\Domain\HealthCheck;

use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\HealthCheck\HealthChecker\HealthChecker;
use Talentry\HealthCheck\HealthReport\HealthReport;
use Talentry\HealthCheck\HealthReport\HealthReportBuilder;

class MessageBrokerHealthChecker implements HealthChecker
{
    //TODO make configurable in bundle
    private const string HEALTH_CHECK_CHANNEL_NAME = 'HEALTH_CHECK';

    public function __construct(
        private readonly MessagePublisher $messagePublisher,
        private readonly MessageSubscriber $messageSubscriber,
        private readonly LoggerInterface $logger = new NullLogger(),
    ) {
    }

    public function getServiceName(): string
    {
        return 'message-broker';
    }

    public function getHealthReport(): HealthReport
    {
        $builder = new HealthReportBuilder();
        $builder->setServiceName($this->getServiceName());
        $builder->setDetails([
            'implementation' => sprintf(
                '%s-%s',
                $this->messagePublisher->name(),
                $this->messageSubscriber->name()
            )
        ]);

        try {
            $channel = new Channel(self::HEALTH_CHECK_CHANNEL_NAME);
            $messageId = uniqid();
            $message = new Message($channel, '', $messageId);
            $this->messagePublisher->sendMessage($message);
            $this->messageSubscriber->getMessageFromChannel($channel);
            $builder->setHealthy();
        } catch (Exception $e) {
            $builder->setUnhealthy();
            $this->logger->error($e->getMessage(), ['exception' => $e]);
        }

        return $builder->buildHealthReport();
    }
}
