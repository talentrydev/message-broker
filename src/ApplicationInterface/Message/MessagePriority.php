<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\Message;

enum MessagePriority: string
{
    case NORMAL = 'normal';
    case LOW = 'low';
}
