<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\Message;

use Exception;

class MessageTooLargeException extends Exception
{
}
