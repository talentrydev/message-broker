<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\Message;

use Throwable;

interface UnsupportedMessageException extends Throwable
{
}
