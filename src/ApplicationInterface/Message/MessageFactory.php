<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\Message;

use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message as MessageImplementation;

class MessageFactory
{
    public static function generate(Channel $channel, string $payload, string $payloadId): Message
    {
        return new MessageImplementation($channel, $payload, $payloadId);
    }
}
