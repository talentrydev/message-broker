<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\Message;

use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;

interface Message
{
    public function getId(): string;
    public function getPayload(): string;
    public function getPayloadId(): string;
    public function getChannel(): Channel;
    public function getChannelName(): string;
}
