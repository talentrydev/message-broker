<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\WaitTime;

interface WaitTime
{
    public function getWaitTime(int $maximumWaitTime): int;
}
