<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\WaitTime;

use Talentry\MessageBroker\Domain\Message\WaitTime as WaitTimeImplementation;

class WaitTimeFactory
{
    public static function inSeconds(int $seconds): WaitTime
    {
        return WaitTimeImplementation::inSeconds($seconds);
    }

    public static function maximumValue(): WaitTime
    {
        return WaitTimeImplementation::maximumValue();
    }

    public static function minimumValue(): WaitTime
    {
        return WaitTimeImplementation::minimumValue();
    }

    public static function defaultValue(): WaitTime
    {
        return WaitTimeImplementation::defaultValue();
    }
}
