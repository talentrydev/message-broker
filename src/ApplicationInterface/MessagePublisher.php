<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;

interface MessagePublisher
{
    public function sendMessage(Message $message): void;
    public function getFailedMessageSpool(): FailedMessageSpool;
    public function name(): string;

    /**
     * @param Message[] $messages
     */
    public function batchSendMessages(array $messages): void;
}
