<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\Channel;

interface ChannelFactory
{
    public function createChannel(string $name): Channel;
}
