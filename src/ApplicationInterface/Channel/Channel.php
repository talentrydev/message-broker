<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\Channel;

interface Channel
{
    public function getName(): string;
}
