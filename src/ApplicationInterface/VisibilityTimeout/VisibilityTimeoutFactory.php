<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\VisibilityTimeout;

use Talentry\MessageBroker\Domain\Message\VisibilityTimeout as VisibilityTimeoutImplementation;

class VisibilityTimeoutFactory
{
    public static function inSeconds(int $seconds): VisibilityTimeout
    {
        return VisibilityTimeoutImplementation::inSeconds($seconds);
    }

    public static function maximumValue(): VisibilityTimeout
    {
        return VisibilityTimeoutImplementation::maximumValue();
    }

    public static function minimumValue(): VisibilityTimeout
    {
        return VisibilityTimeoutImplementation::minimumValue();
    }

    public static function defaultValue(): VisibilityTimeout
    {
        return VisibilityTimeoutImplementation::defaultValue();
    }
}
