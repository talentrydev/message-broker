<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\VisibilityTimeout;

interface VisibilityTimeout
{
    public function getVisibilityTimeout(int $maximumVisibilityTimeout): int;
}
