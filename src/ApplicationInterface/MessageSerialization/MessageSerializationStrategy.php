<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface\MessageSerialization;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;

interface MessageSerializationStrategy
{
    public function serialize(Message $message): string;
    public function deserialize(string $data): Message;
}
