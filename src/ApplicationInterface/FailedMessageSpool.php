<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;

interface FailedMessageSpool
{
    /**
     * Pushes the Message onto the end of the queue.
     */
    public function push(Message $message): void;

    /**
     * Pops the message from the beginning of the queue.
     */
    public function pop(): ?Message;
}
