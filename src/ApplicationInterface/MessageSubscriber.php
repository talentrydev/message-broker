<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface;

use Talentry\MessageBroker\ApplicationInterface\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\VisibilityTimeout\VisibilityTimeout;
use Talentry\MessageBroker\ApplicationInterface\WaitTime\WaitTime;

interface MessageSubscriber
{
    public function getMessageFromChannel(
        Channel $channel,
        bool $delete = true,
        ?WaitTime $waitTime = null,
        ?VisibilityTimeout $visibilityTimeout = null,
    ): ?Message;

    /**
     * @return Message[]
     */
    public function getAllMessagesFromChannel(
        Channel $channel,
        bool $delete = false,
    ): array;

    public function deleteMessage(Message $message, Channel $channel): void;

    public function name(): string;
}
