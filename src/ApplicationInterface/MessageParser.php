<?php

declare(strict_types=1);

namespace Talentry\MessageBroker\ApplicationInterface;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\Message\UnsupportedMessageException;

interface MessageParser
{
    /**
     * @throws UnsupportedMessageException
     */
    public function getTenantId(Message $message): ?string;

    /**
     * @throws UnsupportedMessageException
     */
    public function getMessageType(Message $message): string;

    /**
     * @throws UnsupportedMessageException
     */
    public function getKibanaLink(Message $message): string;

    public function supports(Message $message): bool;

    /**
     * @throws UnsupportedMessageException
     */
    public function parse(Message $message): object|string;
}
